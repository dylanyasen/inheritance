﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]

public abstract class Entity : MonoBehaviour
{
    public string entityName;
    public int ID;
    public EntityStats m_stats;
    public Transform m_trans { get; private set; }
    public Animator m_anim { get; private set; }
    public string m_tag { get; private set; }
    public bool m_isDead { get; private set; }

    public AudioSource audioSource { get; private set; }

    protected virtual void Awake()
    {
        m_stats.Init(this);
        m_anim = GetComponent<Animator>();
        m_trans = transform;
        m_tag = gameObject.tag;
        m_isDead = false;

        // get audio source
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
            audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.playOnAwake = false;
    }

    public virtual void Die()
    {
        // TODO:
        // disable collider box
        // play anim & sfx
        // give back to object pool
        m_isDead = true;

        //Show(false);
    }

    public virtual void ApplyDamage(int amt)
    {
        if (m_isDead)
            return;

        m_stats.LoseHP(amt);

        // float text
        string txt = "- " + amt.ToString();
        GM.instance.FloatText(txt, Camera.main.WorldToScreenPoint(m_trans.position), Color.red);
    }

    /// <summary>
    /// Accessors
    /// </summary>
    public void SetPos(Vector2 pos)
    {
        m_trans.position = pos;
    }

    public virtual Vector2 GetPos()
    {
        return m_trans.position;
    }

    public void ChangePosBy(float x, float y)
    {
        Vector2 pos = GetPos();

        pos.x += x;
        pos.y += y;

        SetPos(pos);
    }

    public void ChangePosBy(Vector2 deltaVec)
    {
        Vector2 pos = GetPos();

        pos.x += deltaVec.x;
        pos.y += deltaVec.y;

        SetPos(pos);
    }

    public void Show(bool show = true)
    {
        gameObject.SetActive(show);
    }
}
