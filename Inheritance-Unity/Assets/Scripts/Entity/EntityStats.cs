﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class EntityStats
{
    [SerializeField]
    public int
        MaxHP;
    public int MaxSP;
    public int BaseDMG;
    public int BaseDEF;
    public int MoveSpeed = 1;

    public int cur_hp { get; private set; }
    public int cur_sp { get; private set; }
    public int cur_dmg { get; private set; }
    public int cur_def { get; private set; }
    public int extra_DMG { get; private set; }
    public int extra_DEF { get; private set; }

    private Entity m_entity;

    public void Init(Entity e)
    {
        m_entity = e;

        ResetStats();

        UpdateCurrentDMG();
        UpdateStatsGUI();


        //Debug.Log("init stats");
    }

    private void UpdateCurrentDMG()
    {
        cur_dmg = BaseDMG + extra_DMG;
    }

    private void UpdateCurrentDEF()
    {
        cur_def = BaseDEF + extra_DEF;
    }



    // ==========================
    //       Accessors
    // ==========================
    public void LoseHP(int amt)
    {
        if (amt > 0 && cur_hp > 0)
            cur_hp -= amt;

        if (cur_hp <= 0)
            m_entity.Die();

        Debug.Log("lost " + amt + " hp, " + cur_hp + " left.");

        UpdateStatsGUI();
    }

    public void AddHP(int amt)
    {
        // limit hp addition
        int addAmt = Mathf.Clamp(amt, 0, MaxHP - cur_hp);

        cur_hp += addAmt;

        UpdateStatsGUI();
    }

    public bool LoseSP(int amt)
    {
        if (cur_sp >= amt)
        {
            cur_sp -= amt;
            UpdateStatsGUI();
            return true;
        }

        Debug.Log("not enough sp");
        return false;
    }

    public void AddSP(int amt)
    {
        int addAmt = Mathf.Clamp(amt, 0, MaxSP - cur_sp);

        cur_sp += addAmt;
        UpdateStatsGUI();
    }

    public void SetExtraDMG(int dmg)
    {
        extra_DMG = dmg;

        if (extra_DMG < 0)
            extra_DMG = 0;

        UpdateCurrentDMG();
    }

    public void AddExtraDMG(int dmg)
    {
        extra_DMG += dmg;

        if (extra_DMG < 0)
            extra_DMG = 0;

        UpdateCurrentDMG();
    }

    public void SetExtraDEF(int def)
    {
        extra_DEF = def;

        if (extra_DEF < 0)
            extra_DEF = 0;

        UpdateCurrentDEF();
    }

    public void AddExtraDEF(int def)
    {
        extra_DEF += def;

        if (extra_DEF < 0)
            extra_DEF = 0;

        UpdateCurrentDEF();
    }

    /// <summary>
    /// Reset stats to default
    /// </summary>
    public void ResetStats()
    {
        cur_hp = MaxHP;
        cur_sp = MaxSP;
    }

    public void UpdateStatsGUI()
    {
        // update player stats gui
        if (m_entity is Player)
        {
            Player p = (Player)m_entity;

            if (p.statsGUI)
            {
                //Debug.Log("update gui");
                p.statsGUI.UpdateSp(cur_sp);
                p.statsGUI.UpdateHp(cur_hp);
            }
        }

        else if (m_entity is Boss)
        {
            Boss b = (Boss)m_entity;

            if (b.statsGUI)
            {
                b.statsGUI.UpdateHp(cur_hp);
            }
        }
    }
}
