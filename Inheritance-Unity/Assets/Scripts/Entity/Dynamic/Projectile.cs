﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ProjectileController))]

public class Projectile : DynamicEntity
{
    public ProjectileController m_controller { get; private set; }

    // might need to extend to base entity later (for use of static entity)
    public DynamicEntity _ownerEntity { get; private set; }

    private TrailRenderer m_trail;
    private SpriteRenderer m_renderer;

    protected override void Awake()
    {
        base.Awake();
        m_controller = (ProjectileController)_controller;

        m_renderer = GetComponent<SpriteRenderer>();
        m_trail = GetComponent<TrailRenderer>();

        m_trail.sortingLayerID = m_renderer.sortingLayerID;
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);

        if (triggeredObjTag == "obstacle")
            Destroy(gameObject);

        else if (m_tag != triggeredObjTag)
        {
            triggeredEntity.ApplyDamage(m_stats.cur_dmg);
        }

    }

    public void SetOwnerEntity(DynamicEntity entity)
    {
        this._ownerEntity = entity;
    }
}
