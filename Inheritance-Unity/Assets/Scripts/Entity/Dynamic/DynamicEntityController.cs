﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(DynamicEntity))]

public class DynamicEntityController : MonoBehaviour
{
    public bool m_isFacingLeft { get; protected set; }
    public bool m_isMoving { get; protected set; }
    public bool m_isAttacking { get; protected set; }
    public bool m_isHurt { get; protected set; }
    public bool m_isDashing { get; protected set; }
    //public bool m_isDead { get; protected set; }
    public bool m_canControl { get; protected set; }
    public bool m_isSpecialAttacking { get; protected set; }

    protected Vector2 m_velocity { get; set; }
    public Animator m_anim { get; private set; }
    protected Rigidbody2D m_body { get; private set; }
    protected Transform m_trans { get; private set; }
    protected DynamicEntity m_entity { get; private set; }
    protected EntityStats m_stats { get; private set; }

    public HitBox hitbox;

    protected Vector2 movement;

    protected virtual void Awake()
    {

        m_entity = GetComponent<DynamicEntity>();

        m_anim = m_entity.m_anim;
        m_body = m_entity.m_body;
        m_stats = m_entity.m_stats;
        m_trans = transform;

        m_isFacingLeft = true;
        m_isMoving = false;
        m_isAttacking = false;
        m_canControl = true;

        // set hit box
        if (hitbox)
            hitbox.SetEntity(m_entity);
    }

    protected virtual void Update()
    {

    }

    public virtual void UnderAttack()
    {
        m_isHurt = true;
        m_anim.SetBool("isHurt", true);
        // other effects

        // push back
        //Vector2 facingDir = m_entity.GetDir();
        //Vector2 pushDir = Vector2.zero;

        //if (facingDir == Vector2.right)
        //    pushDir = -Vector2.right;

        //else if (facingDir == -Vector2.right)
        //    pushDir = Vector2.right;

        //else if (facingDir == Vector2.up)
        //    pushDir = -Vector2.up;

        //else if (facingDir == -Vector2.up)
        //    pushDir = Vector2.up;

        //m_body.AddForce(pushDir * 150, ForceMode2D.Impulse);

        //Debug.Log("pushed! " + pushDir);
    }

    public virtual void UnderAttackEnd()
    {
        m_isHurt = false;
        m_anim.SetBool("isHurt", false);
    }

    protected void FlipSide()
    {
        Vector3 scale = m_trans.localScale;
        scale.x *= -1;
        m_trans.localScale = scale;

        m_isFacingLeft = !m_isFacingLeft;
    }

    public void SetFaceTo(float x, float y)
    {
        movement.Set(x, y);
        m_anim.SetFloat("x", movement.x);
        m_anim.SetFloat("y", movement.y);
    }

    public void ToggleAttackState()
    {
        m_isAttacking = !m_isAttacking;
    }

    public void SetEntity(DynamicEntity entity)
    {
        m_entity = entity;
    }

    public void SetCanControl(bool b)
    {
        m_canControl = b;
    }

    public void SetVelDir(Vector2 velDir)
    {
        m_body.velocity = velDir * m_entity.m_stats.MoveSpeed;
    }

    public void OnDash(float duration = 0.2f)
    {
        m_isDashing = true;
        m_anim.SetBool("isDashing", true);
        m_anim.SetBool("isWalking", false);

        Vector2 dashDir = Vector2.zero;

        Vector2 faceDir = Vector2.zero;
        faceDir.x = m_anim.GetFloat("x");
        faceDir.y = m_anim.GetFloat("y");
        // oppsite direction
        dashDir = faceDir * (-1);

        //Debug.Log(dashDir);

        // set vel here
        // but lerp it in coroutine
        Vector2 vel = dashDir * m_stats.MoveSpeed * 4;

        StartCoroutine(Dash(duration, vel.x, vel.y));
    }

    protected IEnumerator Dash(float dashDur, float x, float y)
    {
        float time = 0;
        Vector2 vel = Vector2.zero;

        while (dashDur > time)
        {
            // update duration timer
            time += Time.deltaTime;

            // smooth vel
            x = Mathf.SmoothDamp(x, 0, ref x, 0.05f);
            y = Mathf.SmoothDamp(y, 0, ref y, 0.05f);
            vel.Set(x, y);

            // set vel
            m_body.velocity = vel;

            yield return 0;
        }


        // set cooldown here
        //yield return new WaitForSeconds(dashCoolDown);

        yield return null;
        EndDash();
    }

    protected void EndDash()
    {
        m_isDashing = false;
        m_anim.SetBool("isDashing", false);
        m_body.velocity = Vector2.zero;
    }

    protected virtual void EnableHitBox(int dir)
    {
        hitbox.EnableHitBox(dir);
    }

    protected virtual void DisableHitBox(int dir)
    {
        hitbox.DisableHitBox(dir);
    }

    public void SetXPos(float x)
    {
        Vector2 v = m_trans.position;
        v.x = x;
        m_trans.position = v;
    }

    public void SetYPos(float y)
    {
        Vector2 v = m_trans.position;
        v.y = y;
        m_trans.position = v;
    }

    public Vector2 GetPos()
    {
        return m_trans.position;
    }

    public void SetVelocity(Vector2 vel)
    {
        m_body.velocity = vel;
    }

    public Vector2 GetFaceDir()
    {
        return new Vector2(m_anim.GetFloat("x"), m_anim.GetFloat("y"));
    }

    public void DisableCollider()
    {
        //Debug.Log("disable collider, count:" + m_entity.colliders.Length);

        foreach (Collider2D col in m_entity.colliders)
            col.enabled = false;
    }

    public void EnableCollider()
    {
        //Debug.Log("enable collider, count:" + m_entity.colliders.Length);

        foreach (Collider2D col in m_entity.colliders)
            col.enabled = true;
    }
}
