﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProjectileController : DynamicEntityController
{
    public enum MotionType
    {
        Circular_Spread,
        Single_Direction,
        Four_Right_Direction
    }

    private Projectile m_projectileEntity;
    private List<Projectile> m_projectiles;

    Vector2 direction;

    protected override void Awake()
    {
        base.Awake();

        m_projectileEntity = (Projectile)m_entity;
        m_projectiles = new List<Projectile>();
    }

    public void Shoot()
    {
        m_body.velocity = direction * m_stats.MoveSpeed;

        for (int i = 0; i < m_projectiles.Count; i++)
            m_projectiles[i].m_controller.Shoot();

        //switch (motionType)
        //{
        //    case MotionType.Circular_Spread:
        //        break;
        //    case MotionType.Single_Direction:
        //        break;
        //    case MotionType.Four_Right_Direction:
        //        break;
        //}
    }

    public void SetMotionType(MotionType type)
    {
        switch (type)
        {
            case MotionType.Circular_Spread:
                CircularSpread();
                break;
            case MotionType.Single_Direction:
                SingleDirection();
                break;
            case MotionType.Four_Right_Direction:
                break;
        }
    }

    private void CircularSpread()
    {
        // set my rotation
        //RotateToDir(m_projectileEntity._ownerEntity._controller.m_dir);

        // set my direction
        SetDirection(GetAbsDir());

        // get my rotation
        // other projectile rotation from this degree
        float firstDeg = GetRotation();

        // create other projectiles
        for (int i = 1; i < 12; i++)
        {
            GameObject g = Instantiate(gameObject, m_trans.position, Quaternion.identity) as GameObject;
            Projectile p = g.GetComponent<Projectile>();

            g.name = i.ToString();

            // rotation to first projectile's deg first
            // p.m_controller.RotateToDegree(firstDeg);

            // unity goes counterclock wise
            //  p.m_controller.RotateToDegree(firstDeg + i * 30);
            //p.m_controller.RotateByDegree(i * 30);
            p.m_controller.SetRotation(firstDeg + i * 30);

            p.m_controller.SetDirection(p.m_controller.GetAbsDir());
            m_projectiles.Add(p);
        }
    }

    private void SingleDirection()
    {
        // set my rotation by on entity direction
        //RotateToDir(m_projectileEntity._ownerEntity._controller.m_dir);

        SetDirection(GetAbsDir());
    }

    public void SetDirection(Vector2 dir)
    {
        direction = dir;
    }

    /*
    void RotateToDir(DIR dir)
    {
        Vector3 euler = new Vector3(0, 0, 0);

        switch (dir)
        {
            // CARE:default projectile sprite should be pointing upward
            case DIR.UP:
                euler.Set(0, 0, 0);
                break;

            case DIR.DOWN:
                euler.Set(0, 0, 180);
                break;

            case DIR.RIGHT:
                euler.Set(0, 0, -90);
                break;

            case DIR.LEFT:
                euler.Set(0, 0, 90);
                break;
        }

        SetRotation(euler);
    }
     */

    float GetRotation()
    {
        return m_trans.rotation.eulerAngles.z;
    }

    void RotateByDegree(float deg)
    {

        Vector3 euler = m_trans.rotation.eulerAngles;
        euler.z += deg;

        //quaternion.eulerAngles = euler;

        //m_trans.rotation = quaternion;
        SetRotation(euler);
    }

    void RotateToDegree(float deg)
    {
        Vector3 euler = new Vector3(0, 0, deg);

        SetRotation(euler);
    }

    Vector2 GetAbsDir()
    {
        float z = GetRotation();

        // *start with 90.
        // since sprite points upwards
        Vector2 v = new Vector2(Mathf.Cos((z + 90) * Mathf.Deg2Rad), Mathf.Sin((z + 90) * Mathf.Deg2Rad));

        return v.normalized;
    }

    void SetRotation(Vector3 euler)
    {
        m_trans.rotation = Quaternion.Euler(euler);
    }

    void SetRotation(float z)
    {
        Vector3 euler = new Vector3(0, 0, z);
        m_trans.rotation = Quaternion.Euler(euler);
    }
}
