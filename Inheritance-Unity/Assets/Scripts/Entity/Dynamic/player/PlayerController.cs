﻿using UnityEngine;
using System.Collections;

public class PlayerController : DynamicEntityController
{
    //private InputManager inputManager;
    //private Vector2 movement;
    private bool isOpenInventory = false;
    private Player m_player;

    private bool isSwing = false;
    private bool isArmed = false;

    protected override void Awake()
    {
        base.Awake();
        //inputManager = GM.instance._inputManager;
        m_player = (Player)m_entity;

        // set hit box tag
        if (hitbox)
            hitbox.SetTag("PlayerHitbox");
    }

    void Start()
    {
        movement = new Vector2();

        // initialy face down
        SetFaceTo(0, -1);
    }

    protected override void Update()
    {


        /*
         *      Special Controls
         */
        if (Input.GetButtonDown("Attack") && !m_isAttacking && m_canControl)
        {
            m_anim.SetBool("isAttacking", true);
            OnAttack();
        }

        else if (Input.GetButtonDown("Backback") && m_canControl)
        {
            isOpenInventory = !isOpenInventory;
            m_player.inventoryPanel.SwithGUI(isOpenInventory);
        }

        else if (Input.GetButtonDown("QuickUse"))
        {
            // quick use    
        }

        else if (Input.GetButtonDown("Spell") && m_canControl)
        {
            if (m_player.spell != null)
                m_player.spell.Invoke();
        }

        // Dash
        // temp
        else if (Input.GetKeyDown(KeyCode.X) && m_canControl)
        {
            // 
            //Debug.Log("dash");
            OnDash();
        }

        /*
         *      Update Movement
         */

        // ==> if(canControl)
        // get input
        float inputX = Input.GetAxisRaw("Horizontal");
        float inputY = Input.GetAxisRaw("Vertical");

        // has key input
        // ==> && not attacking

        if ((inputX != 0 || inputY != 0) && m_canControl && !m_isAttacking && !m_isDashing && !m_entity.m_isDead)
        {
            // update walking m_anim
            m_anim.SetBool("isWalking", true);

            // update blend tree parameters ==> ONLY update when there is keyborad input
            movement.Set(inputX, inputY);

            // update animation
            m_anim.SetFloat("y", movement.y);

            // diagonal input. ONLY keep Y
            if (Mathf.Abs(inputX) == Mathf.Abs(inputY))
                m_anim.SetFloat("x", 0);
            else
                m_anim.SetFloat("x", movement.x);

            // update velocity
            movement.Normalize();
            m_body.velocity = movement * m_stats.MoveSpeed;
        }
        else
        {
            // no input back to idle
            m_anim.SetBool("isWalking", false);

            if (!m_isDashing) // && isHurting for not-reset vel when puch back
                m_body.velocity = Vector2.zero;
        }

        // update armor sprites
        int dir = 0;

        if (m_player.GetDir() == Vector2.right || m_player.GetDir() == -Vector2.right)
            dir = 0;

        else if (m_player.GetDir() == -Vector2.up)
            dir = 1;

        else if (m_player.GetDir() == Vector2.up)
            dir = 2;

        // TODO: move this somewhere else
        // update weapon type
        m_player.armorManager.SwitchSprite(dir);
    }

    void OnAttack()
    {
        m_isAttacking = true;

        float x = m_anim.GetFloat("x");
        float y = m_anim.GetFloat("y");

        m_player.Attack(new Vector2(x, y));
    }

    void EndAttack()
    {
        m_anim.SetBool("isAttacking", false);
        m_isAttacking = false;
    }

    public void SetArmed(bool b)
    {
        isArmed = b;
        m_anim.SetBool("isArmed", b);
    }

    public void SetSwing(bool b)
    {
        isSwing = b;
        m_anim.SetBool("isSwing", b);
    }

    ///=========================================== Accessors==========================================//
    //public void SetFaceTo(float x, float y)
    //{
    //    movement.Set(x, y);
    //    m_anim.SetFloat("x", movement.x);
    //    m_anim.SetFloat("y", movement.y);
    //}

    //protected override void Update()
    //{
    //    base.Update();

    //    if (m_canControl)
    //    {
    //        ProcessInput();
    //    }

    //    Move(dir);

    //    if (!m_isAttacking)
    //        Updatem_animation();

    //    if (Input.GetButtonDown("Attack"))
    //    {
    //        dir = Vector2.zero;
    //        m_m_anim.SetTrigger("attack");
    //    }

    //    else if (Input.GetButtonDown("Backback"))
    //    {
    //        isOpenInventory = !isOpenInventory;
    //        m_player.inventoryPanel.SwithGUI(isOpenInventory);
    //    }

    //    else if (Input.GetButtonDown("QuickUse"))
    //    {
    //        // quick use    
    //    }


    //    // testing
    //    // temp
    //    /*
    //    else if (Input.GetKeyDown(KeyCode.Space))
    //    {
    //        GameObject g = Instantiate(m_entity.projectile, m_trans.position, Quaternion.identity) as GameObject;
    //        Projectile p = g.GetComponent<Projectile>();
    //        p.SetOwnerEntity(m_entity);

    //        p.m_controller.SetMotionType(ProjectileController.MotionType.Circular_Spread);
    //        p.m_controller.Shoot();
    //    }

    //     */
    //}

    //private void ProcessInput()
    //{
    //    KeyCode key = inputManager.latestKey;

    //    switch (key)
    //    {
    //        case KeyCode.None:
    //            dir = Vector2.zero;
    //            m_isMoving = false;
    //            break;

    //        case KeyCode.LeftArrow:
    //            dir = -Vector2.right;
    //            m_dir = DIR.LEFT;
    //            m_isMoving = true;
    //            break;

    //        case KeyCode.RightArrow:
    //            dir = Vector2.right;
    //            m_dir = DIR.RIGHT;
    //            m_isMoving = true;
    //            break;

    //        case KeyCode.UpArrow:
    //            dir = Vector2.up;
    //            m_dir = DIR.UP;
    //            m_isMoving = true;
    //            break;

    //        case KeyCode.DownArrow:
    //            dir = -Vector2.up;
    //            m_dir = DIR.DOWN;
    //            m_isMoving = true;
    //            break;

    //        default:
    //            m_isMoving = false;
    //            break;
    //    }
    //}

    //void Updatem_animation()
    //{
    //    //if (!m_isMoving) return;

    //    switch (m_dir)
    //    {
    //        case DIR.UP:
    //            m_m_anim.SetBool("back", true);
    //            m_m_anim.SetBool("front", false);
    //            m_m_anim.SetBool("side", false);
    //            if (!m_isFacingLeft) FlipSide();
    //            break;

    //        case DIR.DOWN:
    //            m_m_anim.SetBool("front", true);
    //            m_m_anim.SetBool("side", false);
    //            m_m_anim.SetBool("back", false);
    //            if (!m_isFacingLeft) FlipSide();
    //            break;

    //        case DIR.LEFT:
    //            m_m_anim.SetBool("side", true);
    //            m_m_anim.SetBool("back", false);
    //            m_m_anim.SetBool("front", false);
    //            if (!m_isFacingLeft) FlipSide();
    //            break;

    //        case DIR.RIGHT:
    //            m_m_anim.SetBool("side", true);
    //            m_m_anim.SetBool("back", false);
    //            m_m_anim.SetBool("front", false);
    //            if (m_isFacingLeft) FlipSide();
    //            break;
    //    }

    //    m_m_anim.SetBool("isWalking", m_isMoving);
    //}


    //public void TurnTo(DIR dir)
    //{
    //    m_dir = dir;
    //    m_isMoving = false;
    //}

    //public void Triggerm_animation(string triggerName)
    //{
    //    if (triggerName == "hurt")
    //    {
    //        switch (m_dir)
    //        {
    //            case DIR.UP: m_m_anim.SetTrigger("UpHurt"); break;
    //            case DIR.DOWN: m_m_anim.SetTrigger("DownHurt"); break;
    //            case DIR.LEFT: m_m_anim.SetTrigger("SideHurt"); break;
    //            case DIR.RIGHT: m_m_anim.SetTrigger("SideHurt"); break;
    //        }
    //    }
    //}

    //void Move(Vector2 dir)
    //{
    //    m_body.velocity = dir * m_stats.MoveSpeed;
    //}

    //public void SetDir(Vector2 dir)
    //{
    //    this.dir = dir;
    //}

    //void OnAttack()
    //{
    //    m_isAttacking = true;
    //}

    //void EndAttack()
    //{
    //    m_isAttacking = false;
    //}

}
