﻿using UnityEngine;
using System.Collections;

public class Heretic : Player
{
    protected override void Start()
    {
        name = Constants.Heretic;

        base.Start();

        inventory.AddItemByID(4);
        inventory.AddItemByID(5);
        inventory.AddItemByID(22);

    }

}
