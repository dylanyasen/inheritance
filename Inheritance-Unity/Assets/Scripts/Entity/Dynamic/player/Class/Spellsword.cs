﻿using UnityEngine;
using System.Collections;

public class Spellsword : Player
{
    protected override void Start()
    {
        name = Constants.Spellsword;

        base.Start();

        inventory.AddItemByID(16);
        inventory.AddItemByID(17);
        inventory.AddItemByID(20);
    }


}
