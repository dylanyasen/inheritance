﻿using UnityEngine;
using System.Collections;

public class Oracle : Player
{
    protected override void Start()
    {
        name = Constants.Oracle;

        inventory.AddItemByID(0);
        inventory.AddItemByID(1);
        inventory.AddItemByID(19);

        base.Start();
    }

}
