﻿using UnityEngine;
using System.Collections;

public class Exorcist : Player
{
    protected override void Start()
    {
        name = Constants.Exorcist;

        base.Start();

        inventory.AddItemByID(108);
        inventory.AddItemByID(109);
        inventory.AddItemByID(30);
        
    }
}
