﻿using UnityEngine;
using System.Collections;

public class Magician : Player
{
    public GameObject projectile { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        spell = new FireBurst(this);
    }

    protected override void Start()
    {
        name = Constants.Magician;

        base.Start();

        inventory.AddItemByID(14);
        inventory.AddItemByID(15);
        inventory.AddItemByID(28);
    }

    public override void Attack(Vector2 dir)
    {
        if (weaponManager.currentWeapon is RangeWeapon)
            RangeAttack(dir);
    }

    private void RangeAttack(Vector2 dir)
    {
        projectile = ((RangeWeapon)weaponManager.currentWeapon)._projectile;


        GameObject g = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;

        g.GetComponent<ProjectileWithParticle>().SetDmg(m_stats.cur_dmg);
        g.GetComponent<ProjectileWithParticle>().SetSpeed(500);
        g.GetComponent<ProjectileWithParticle>().Fire(dir);
    }
}
