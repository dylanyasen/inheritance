﻿using UnityEngine;
using System.Collections;

public class Paladin : Player
{

    protected override void Start()
    {
        name = Constants.Paladin;

        base.Start();

        inventory.AddItemByID(8);
        inventory.AddItemByID(9);
        inventory.AddItemByID(18);
    }

}
