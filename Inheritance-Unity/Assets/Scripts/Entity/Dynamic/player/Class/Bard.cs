﻿using UnityEngine;
using System.Collections;

public class Bard : Player
{
    protected override void Start()
    {
        name = Constants.Bard;

        inventory.AddItemByID(12);
        inventory.AddItemByID(13);
        inventory.AddItemByID(26);

        base.Start();
    }

}
