﻿using UnityEngine;
using System.Collections;

public class Fighter : Player
{

    protected override void Awake()
    {
        base.Awake();

        spell = new ThrowWeapon(this);
    }

    protected override void Start()
    {
        name = Constants.Barbarian;

        base.Start();

        inventory.AddItemByID(6);
        inventory.AddItemByID(7);

        inventory.AddItemByID(18);
        //inventory.AddItemByID(19);
    }
}
