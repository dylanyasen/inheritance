﻿using UnityEngine;
using System.Collections;

public class Ranger : Player
{
    public GameObject arrow { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        spell = new TripleShot(this);
    }

    protected override void Start()
    {
        name = Constants.Ranger;

        base.Start();

        inventory.AddItemByID(2);
        inventory.AddItemByID(3);
        inventory.AddItemByID(32);

        //inventory.AddItemByID(107);
        //inventory.AddItemByID(47);
        //inventory.AddItemByID(62);
        //inventory.AddItemByID(92);
        //inventory.AddItemByID(62);


        inventory.AddItemByID(110);
        inventory.AddItemByID(111);
        inventory.AddItemByID(112);
        inventory.AddItemByID(113);
        inventory.AddItemByID(114);
        inventory.AddItemByID(115);

    }

    public override void Attack(Vector2 dir)
    {
        base.Attack(dir);
    }
}

