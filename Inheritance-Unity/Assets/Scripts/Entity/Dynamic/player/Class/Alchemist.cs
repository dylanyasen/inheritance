﻿using UnityEngine;
using System.Collections;

public class Alchemist : Player
{
    protected override void Awake()
    {
        base.Awake();

        spell = new PlaceBomb(this);
    }

    protected override void Start()
    {
        name = Constants.Alchemist;

        inventory.AddItemByID(10);
        inventory.AddItemByID(11);
        inventory.AddItemByID(31);

        base.Start();
    }

}
