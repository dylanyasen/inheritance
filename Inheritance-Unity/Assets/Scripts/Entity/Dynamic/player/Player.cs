﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Traits
{
    Miserly,
    Knowledgeable,
    Hardy,
    Hasty,
    FastMetabolism,
    Prodigy
}
//[RequireComponent(typeof(DynamicEntityController))]
public abstract class Player : DynamicEntity
{
    // TODO: GM should be the only singelton
    public static Player instance;

    public Traits trait;
    // multi-traits
    // public List<Traits> traits = new List<Traits>();

    public WeaponManager weaponManager { get; private set; }
    public ArmorManager armorManager { get; private set; }


    //private PlayerTestingContoller m_controller;
    public PlayerController m_controller { get; private set; }

    // inventory ref
    public InventoryPanel inventoryPanel { get; private set; }
    public Inventory inventory { get; private set; }
    public CharPanel charPanel { get; private set; } // might not need this

    // stats gui
    public PlayerStatGui statsGUI { get; private set; }

    public Spell spell { get; protected set; }

    public int goldAmt { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        m_controller = (PlayerController)_controller;

        weaponManager = GetComponent<WeaponManager>();
        armorManager = GetComponent<ArmorManager>();

        InitInventory();
    }

    protected virtual void Start()
    {
        Sprite classPotrait = ItemDatabase.instance.classPortraitSpriteSheet[name];
        //GM.instance.ingame_gui.SetPlayer(this);
        GM.instance.ingame_gui.SetFace(classPotrait);
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(bound.center, bound.size);
    }

    private void InitInventory()
    {
        inventoryPanel = InventoryPanel.instance;
        Debug.Log(inventoryPanel);
        inventory = inventoryPanel.inventory;
        charPanel = inventoryPanel.charPanel;
    }

    protected override void OnCollisionEnter2D(Collision2D other)
    {


        //Debug.Log("hit");

        //if (collidedObjTag == "Enemy")
        // TODO: maybe push back 
    }

    public override void ApplyDamage(int amt)
    {
        base.ApplyDamage(amt);

        m_controller.UnderAttack();
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        triggeredObj = other.gameObject;
        triggeredObjTag = triggeredObj.tag;
        triggeredObjLayer = triggeredObj.layer;
        triggeredEntity = other.GetComponent<DynamicEntity>();

        // enemy weapon collision
        if (triggeredObjTag == "EnemyHitbox")
        {
            Debug.Log("enemy hit player");
            ApplyDamage(other.transform.parent.GetComponent<HitBox>().m_entity.m_stats.cur_dmg);
        }
    }

    public override void Die()
    {
        // face down
        m_controller.SetFaceTo(0, -1);

        // destroy
        base.Die();

        // disable camera follow
        GM.instance._camController.SwitchIsFollowing(false);

        //Destroy(gameObject);
    }

    public void OnDeathAnimEnd()
    {

        GameObject g = Instantiate(Resources.Load<GameObject>(Constants.EntityPrefabPath + "PlayerCorpse"), GetPos(), Quaternion.identity) as GameObject;
        // TODO: Preload in Resource-Manager
        g.GetComponent<PlayerCorpse>().SetItems(inventoryPanel.GetAllItems());
        g.GetComponent<PlayerCorpse>().Save();


        // offset a little bit in Y axis
        // for death overlay
        Vector2 pos = GetPos();
        pos.y += 150;
        GM.instance._camController.GoToPos(pos);

        // TODO wait a little bit?
        gameObject.SetActive(false);

        GM.instance.GameOver();
    }

    public void AddItemToInventory(Item item)
    {
        //Debug.Log(inventory);

        //if(inventory NOT FULL)

        inventory.AddItem(item);
    }

    public void AddItemToInventory(int id)
    {
        //Debug.Log(inventory);

        //if(inventory NOT FULL)

        inventory.AddItemByID(id);
    }

    public void AddGold(int amt)
    {
        goldAmt += amt;
        UpdateGoldGUI();
    }

    public bool SpendGold(int amt)
    {
        if (goldAmt >= amt)
        {
            goldAmt -= amt;
            UpdateGoldGUI();
            return true;
        }

        return false;
    }

    private void UpdateGoldGUI()
    {
        inventoryPanel.goldLabel.text = goldAmt.ToString();
    }

    public void SetInventory(InventoryPanel panel)
    {
        inventoryPanel = panel;
        inventory = panel.inventory;
        charPanel = panel.charPanel;
    }

    public void SetStatsGUI(PlayerStatGui gui)
    {
        statsGUI = gui;
        gui.SetPlayer(this);
        m_stats.UpdateStatsGUI();
    }

    public virtual void Attack(Vector2 dir)
    {
        if (weaponManager.currentWeapon != null)
            weaponManager.currentWeapon.Attack(dir, this);
    }
}
