﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[RequireComponent(typeof(EnemyController))]
public class Enemy : DynamicEntity
{
    // TODO: Downcast from parent
    public EnemyController m_controller { get; private set; }
    public bool m_isCollidingPlayer { get; private set; }

    public AIEntity m_aiEntity { get; private set; }

    public int collideObjDir { get; private set; } // * 1 for up; 2 for right; 3 for down; 4 for left;

    public List<Loot> loot = new List<Loot>();

    public int goldWorth = 50;

    //public GameObject projectile;

    protected override void Awake()
    {
        base.Awake();
        //m_controller = (EnemyController)m_controller;
        //gameObject.GetComponent<EnemyController>() as EnemyController;
        //m_controller = (EnemyController)m_controller;

        m_controller = (EnemyController)_controller;

        m_aiEntity = GetComponent<AIEntity>();
        gameObject.tag = "Enemy";
        gameObject.layer = LayerMask.NameToLayer("enemy");
    }

    protected virtual void Start()
    {
        //m_aiEntity.target = GM.instance._player.transform;
        m_aiEntity.target = Player.instance.m_trans;
    }

    protected override void OnCollisionEnter2D(Collision2D other)
    {
        base.OnCollisionEnter2D(other);

        // CAREFUL: obstacle doesnt have entity script
        if (collidedObjLayer == LayerMask.NameToLayer("obstacle"))
        {
            //Debug.Log("hit obstacle");

            m_controller.TurnAround();
        }

        //if (collidedObjTag == "Player")
        //{
        //    m_isCollidingPlayer = true;

        //    // to avoid pushing
        //    //m_body.isKinematic = true;

        //    //TODO: cache this 
        //    Vector2 playerPos = collidedObj.transform.position;
        //    Vector2 m_pos = transform.position;

        //    /*  1 for up; 
        //        2 for right; 
        //        3 for down; 
        //        4 for left;
        //     */

        //    // direction of player
        //    Vector2 dir = collidedObj.transform.position - transform.position;
        //    float dirRad = Mathf.Atan2(dir.y, dir.x);

        //    if (Mathf.Abs(dirRad) < 0.707f)
        //        collideObjDir = 2;
        //    else if (dirRad >= 0.707f && dirRad < 2.356f)
        //        collideObjDir = 1;
        //    else if (Mathf.Abs(dirRad) > 2.356f)
        //        collideObjDir = 4;
        //    else
        //        collideObjDir = 3;
        //}
    }

    protected override void OnCollisionExit2D(Collision2D other)
    {
        base.OnCollisionExit2D(other);

        //if (collidedObjTag == "Player")
        //{
        //    m_isCollidingPlayer = false;

        //    // return back to normal physics body
        //    //m_body.isKinematic = true;
        //}
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (m_isDead)
            return;

        triggeredObj = other.gameObject;
        triggeredObjTag = triggeredObj.tag;
        triggeredObjLayer = triggeredObj.layer;
        triggeredEntity = other.GetComponent<Entity>();

        // player weapon collision
        if (triggeredObjTag == "PlayerHitbox")
            ApplyDamage(other.transform.parent.GetComponent<HitBox>().m_entity.m_stats.cur_dmg);
    }

    public override void ApplyDamage(int amt)
    {
        base.ApplyDamage(amt);

        m_aiEntity.FSM.ChangeState(new UnderAttackState(m_aiEntity), true);
        // end state in enemy controller ->UnderAttackEnd();
    }

    public override void Die()
    {
        base.Die();

        Debug.Log("die");


        // item drop
        foreach (Loot lt in loot)
        {
            if (lt.Drop())
            {
                GameObject pickup = Instantiate(Resources.Load<GameObject>(Constants.EntityPrefabPath + "Pickup"), m_trans.position, Quaternion.identity) as GameObject;
                pickup.GetComponent<Pickup>().Init(lt.itemID);
                Debug.Log("drop");
            }
        }

        // drop gold
        GameObject gold = Instantiate(Resources.Load<GameObject>(Constants.EntityPrefabPath + "Goldbag"), m_trans.position, Quaternion.identity) as GameObject;
        Goldbag goldBag = gold.GetComponent<Goldbag>();
        goldBag.SetAmt(goldWorth);
    }
}
