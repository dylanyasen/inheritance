﻿using UnityEngine;
using System.Collections;

public class BrainWormController : BossController
{
    public GameObject summonPrefab { get; private set; }

    protected override void Awake()
    {
        base.Awake();


        Debug.Log("BrainWormController  awake");
    }


    protected override void Start()
    {
        base.Start();

        summonPrefab = Resources.Load<GameObject>(Constants.HazardPrefabPath + "Tentacle");
    }

    public override void Summon(Vector2 pos)
    {

        GameObject tantecle = GameObject.Instantiate(summonPrefab, pos, Quaternion.identity) as GameObject;

        // trigger tantecle
    }
}
