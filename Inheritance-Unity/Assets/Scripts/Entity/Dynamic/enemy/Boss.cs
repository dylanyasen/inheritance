﻿using UnityEngine;
using System.Collections;

public class Boss : Enemy
{
    // stats gui
    public BossStatGui statsGUI { get; private set; }

    public Boss m_boss { get; private set; }

    protected override void Awake()
    {
        base.Awake();
    }

    public override void Die()
    {
        base.Die();

        GM.instance._player.m_controller.SetFaceTo(0, -1);
        GM.instance._player.m_anim.SetTrigger("isCheer");

        Destroy(statsGUI.gameObject);
        //Destroy(gameObject);
    }

    public void SetStatGUI(BossStatGui gui)
    {
        statsGUI = gui;
    }

}
