﻿using UnityEngine;
using System.Collections;

public class Worm : AIEntity
{
    protected override void Start()
    {
        base.Start();
        FSM.ChangeState(new PatrolState(this, target), true);
    }

    protected override void Update()
    {
        base.Update();

        // add type-specific behaviors
    }

    public override void Idle()
    {
        //Debug.Log("this is melee simple idle");
        m_controller.Idle();
    }

    public override void Patrol()
    {
        m_controller.MoveRand4Dir();
    }

    public override void Attack()
    {
        base.Attack();
        m_controller.RangeAttack();
    }

    public override void UnderAttack()
    {
        //Debug.Log("this is melee simple under attack");
        m_controller.UnderAttack();
    }

}
