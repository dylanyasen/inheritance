﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BrainWormController))]
public class BrainWorm : AIEntity
{
    private bool canSummon;

   

    //protected override void Update()
    //{
    //    //base.Update();

    //    // no target
    //    if (target == null)
    //    {
    //        if (!(FSM.currentState is IdleState) && !(FSM.currentState is PatrolState))
    //        {
    //            FSM.ChangeState(new IdleState(this), true);

    //            Debug.Log("target null");
    //        }

    //        return;
    //    }
    //}

    public override void Alert()
    {
        Debug.Log("this is melee simple alert");
    }
    
    public override void Attack()
    {
        base.Attack();

        // randomize attack type
        m_controller.Summon();
    }
    
    public override void UnderAttack()
    {
        Debug.Log("this is melee simple under attack");
    }

    public override void Patrol()
    {
        Debug.Log("this is melee simple patrol");
    }

    public override void Idle()
    {
        Debug.Log("this is melee simple idle");
        // m_controller.Idle();
    }

    public override void Chase(Transform target)
    {
        Debug.Log("this is melee simple chase");
        //m_controller.Chase(target, 2);
    }


}
