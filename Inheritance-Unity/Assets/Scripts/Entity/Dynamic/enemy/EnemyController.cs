﻿using UnityEngine;
using System.Collections;

public class EnemyController : DynamicEntityController
{
    protected Enemy m_enemy;
    //protected Vector2 movement;

    protected Vector2 virtualInput;

    protected int[] randDirMap = { 0, -1, 1 };
    protected Vector2[] rand4DirMap = { Vector2.up, -Vector2.up, Vector2.right, -Vector2.right };

    public bool resetVelocity = true;
    // prevent velocity gets override in update
    // when excuting some ai behaviors

    protected override void Awake()
    {
        base.Awake();

        m_enemy = GetComponent<Enemy>();

        // set hit box tag
        if (hitbox)
            hitbox.SetTag("EnemyHitbox");

        // initial facing
        SetFaceTo(0, -1);
    }

    void Start()
    {

        //m_enemy = (Enemy)m_entity;
    }

    protected override void Update()
    {
        base.Update();

        // *VELOCITY instead of user input
        //float inputX = m_body.velocity.x / m_body.velocity.x;
        //float inputY = m_body.velocity.y / m_body.velocity.y;

        float inputX = virtualInput.x;
        float inputY = virtualInput.y;

        if ((inputX != 0 || inputY != 0) && !m_isAttacking && !m_isDashing && !m_isHurt)
        {
            // update walking m_anim
            m_anim.SetBool("isWalking", true);

            // update blend tree parameters ==> ONLY update when there is input
            movement.Set(inputX, inputY);

            // update animation
            m_anim.SetFloat("y", movement.y);

            // diagonal input. ONLY keep Y
            if (Mathf.Abs(inputX) == Mathf.Abs(inputY))
                m_anim.SetFloat("x", 0);
            else
                m_anim.SetFloat("x", movement.x);
        }
        else
        {
            m_anim.SetBool("isWalking", false);

            if (resetVelocity)
                m_body.velocity = Vector2.zero;
        }
    }

    public void MoveInRandDir()
    {
        // generate random dir
        int i = Random.Range(0, randDirMap.Length);
        virtualInput.x = randDirMap[i];

        i = Random.Range(0, randDirMap.Length);
        virtualInput.y = randDirMap[i];

        // set velocity
        Vector2 vel = virtualInput.normalized;
        m_body.velocity = vel * m_stats.MoveSpeed;

        // anim can't go diaginally 

        //m_anim.SetBool("isWalking", true);

        //Vector2 vel = Random.insideUnitCircle * m_stats.MoveSpeed;
        //m_body.velocity = vel;


        //m_anim.SetFloat("x", vel.x);
        //m_anim.SetFloat("y", vel.y);
        //m_body.velocity = Utility.GetRandUnitVec(Utility.GetRandomDegInRad()) * m_stats.MoveSpeed;
    }


    public override void UnderAttackEnd()
    {
        Debug.Log("under attack end");

        base.UnderAttackEnd();

        m_enemy.m_aiEntity.FSM.currentState.ExitToNextState();
    }


    public void UnderAttackFinished()
    {
        m_isHurt = false;
        m_anim.SetBool("isHurt", false);
        m_enemy.m_aiEntity.FSM.currentState.ExitToNextState();
    }


    public void MoveRand4Dir()
    {
        // generate random dir
        int i = Random.Range(0, rand4DirMap.Length);
        virtualInput.x = rand4DirMap[i].x;
        virtualInput.y = rand4DirMap[i].y;

        // set velocity
        Vector2 vel = virtualInput.normalized;
        m_body.velocity = vel * m_stats.MoveSpeed;
    }


    public void Idle()
    {
        // reset virtual input & velocity
        virtualInput = Vector2.zero;
    }

    public virtual void TurnAround()
    {
        //Debug.Log("turn");

        if (m_isAttacking)
            MoveInRandDir();
    }

    public virtual void MeleeAttack(Transform target)
    {

        // set attack direction
        Level curLvl = GM.instance._levelManager.GetCurrentLevel();

        Vector2 myIndex = curLvl.GetCurrentTileIndex(m_entity.m_trans.position);
        Vector2 targetIndex = curLvl.GetCurrentTileIndex(target.position);

        // above target
        if (myIndex.y < targetIndex.y)
        {
            m_anim.SetFloat("x", 0);
            m_anim.SetFloat("y", 1);
        }
        // below target
        else if (myIndex.y > targetIndex.y)
        {
            m_anim.SetFloat("x", 0);
            m_anim.SetFloat("y", -1);
        }
        // same level
        else if (myIndex.y == targetIndex.y)
        {
            // left side of target 
            if (myIndex.x < targetIndex.x)
            {
                m_anim.SetFloat("x", 1);
                m_anim.SetFloat("y", 0);
            }

            // right side of target
            else
            {
                m_anim.SetFloat("x", -1);
                m_anim.SetFloat("y", 0);
            }
        }

        m_isAttacking = true;
        m_anim.SetBool("isWalking", false);
        m_anim.SetBool("isAttacking", true);
    }

    public virtual void RangeAttack()
    {
        m_isAttacking = true;

        m_anim.SetBool("isWalking", false);
        m_anim.SetBool("isAttacking", true);
    }

    public virtual void Summon()
    {
        //m_anim.SetTrigger("summon");

        m_enemy.collidedEntity.ApplyDamage(m_stats.cur_dmg);
    }

    public virtual void Summon(Vector2 pos)
    {
        //m_anim.SetTrigger("summon");

        //m_enemy.collidedEntity.ApplyDamage(m_stats.cur_dmg);
    }

    public virtual void Jump()
    {
        m_anim.SetBool("isSpecialAttack", true);
        m_isSpecialAttacking = true;

        Vector2 playerPos = m_enemy.m_aiEntity.target.transform.position;
        Vector2 jumpVec = playerPos - (Vector2)m_trans.position;
        jumpVec.Normalize();

        StartCoroutine(OnJump(0.5f, jumpVec * 500));
    }

    IEnumerator OnJump(float duration, Vector2 vel)
    {
        float timer = 0;
        Vector2 vec = Vector2.zero;
        while (timer < duration)
        {
            yield return new WaitForEndOfFrame();

            timer += Time.deltaTime;
            vec = m_trans.position;
            vec.x += vel.x;
            vec.y += vel.y;

            m_body.MovePosition(vec * Time.deltaTime);
        }

        Debug.Log("jump times up!");

        m_isSpecialAttacking = false;
        m_anim.SetBool("isSpecialAttack", false);
        m_body.velocity = Vector2.zero;

        yield return null;
    }

    //void OnCauseDMG()
    //{
    //    // TODO: randomize dmg
    //    if (m_enemy != null && m_enemy.collidedEntity != null)
    //      m_enemy.collidedEntity.ApplyDamage(m_stats.cur_dmg);
    //}

    protected virtual void OnAttackFinished()
    {
        m_isAttacking = false;

        m_anim.SetBool("isAttacking", false);
    }

    public void Chase(Transform target, int speedMultiplier = 1)
    {
        Vector3 dir = (target.position - m_trans.position).normalized;
        //m_body.MovePosition(target.position * m_stats.MoveSpeed * Time.deltaTime);

        m_body.velocity = dir * m_stats.MoveSpeed * speedMultiplier;
        m_anim.SetBool("isWalking", true);
    }


    public virtual void SpecialAttack()
    {
        m_anim.SetBool("isSpecialAttack", true);
        m_isSpecialAttacking = true;

    }

    public void SpecialAttackEnd()
    {
        Debug.Log("special attack ends");
        m_anim.SetBool("isSpecialAttack", false);
        m_isSpecialAttacking = false;
    }

}
