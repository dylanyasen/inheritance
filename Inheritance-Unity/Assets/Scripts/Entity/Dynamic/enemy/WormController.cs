﻿using UnityEngine;
using System.Collections;

public class WormController : EnemyController
{
    GameObject projectile;

    void Start()
    {
        projectile = Resources.Load<GameObject>(Constants.ProjectilePrefabPath + "Bolt Blood Small");
    }

    public override void RangeAttack()
    {
        base.RangeAttack();

        float deg = 0;

        StartCoroutine(Shoot(deg));
    }

    IEnumerator Shoot(float deg)
    {
        for (int i = 0; i < 12; i++)
        {
            GameObject proj = Instantiate(projectile, m_trans.position, Quaternion.Euler(0, 0, deg)) as GameObject;
            ProjectileWithParticle p = proj.GetComponent<ProjectileWithParticle>();
            p.SetDmg(m_stats.cur_dmg);
            p.SetEnemyProj();
            p.Fire();
            deg += 60;

            yield return new WaitForSeconds(0.05f);
        }
    }

    public override void TurnAround()
    {
        //base.TurnAround();
        //if (m_isAttacking)
        // MoveRand4Dir();
    }

}
