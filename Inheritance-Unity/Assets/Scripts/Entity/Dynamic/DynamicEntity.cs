﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]

public abstract class DynamicEntity : Entity
{
    public Rigidbody2D m_body { get; private set; }

    /* abstract out a generic controller class later */
    public DynamicEntityController _controller { get; private set; }

    // move to generic entity later
    public GameObject collidedObj { get; protected set; }
    public Entity collidedEntity { get; protected set; }
    public string collidedObjTag { get; protected set; }
    public LayerMask collidedObjLayer { get; protected set; }

    public GameObject triggeredObj { get; protected set; }
    public Entity triggeredEntity { get; protected set; }
    public string triggeredObjTag { get; protected set; }
    public LayerMask triggeredObjLayer { get; protected set; }

    public Collider2D[] colliders { get; private set; }

    public bool canControl { get; set; }

    public Bounds bound { get; protected set; }

    protected override void Awake()
    {
        base.Awake();
        m_body = GetComponent<Rigidbody2D>();
        _controller = GetComponent<DynamicEntityController>();

        colliders = GetComponents<Collider2D>();
    }

    protected virtual void OnCollisionEnter2D(Collision2D other)
    {
        if (m_isDead)
            return;

        collidedObj = other.gameObject;
        collidedObjTag = collidedObj.tag;
        collidedObjLayer = collidedObj.layer;
        collidedEntity = collidedObj.GetComponent<Entity>();

        //Debug.Log("dynamic entity base collision enter");
    }

    protected virtual void OnCollisionExit2D(Collision2D other)
    {
        if (m_isDead)
            return;
        //Debug.Log ("dynamic entity base collision exit");
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
    }

    protected virtual void OnTriggerExit2D(Collider2D other)
    {
    }

    void Update()
    {
        bound = new Bounds(m_trans.position, Vector2.one * 100);
    }

    public Bounds GetBound()
    {
        return bound;
    }

    public override void ApplyDamage(int amt)
    {
        base.ApplyDamage(amt);

        // reset attacking state
        // * got attacked when attacking will cause player stuck.
        // * since isAttacking is reset in frame event at the end of the animation
        // * so we reset here

        if (_controller.m_isAttacking)
            _controller.ToggleAttackState();

        _controller.UnderAttack();
    }

    public Vector2 GetDir()
    {
        float x = m_anim.GetFloat("x");
        float y = m_anim.GetFloat("y");

        return new Vector2(x, y);
    }

    public override void Die()
    {
        base.Die();
        m_anim.SetBool("isDead", true);
        _controller.DisableCollider();
    }

}
