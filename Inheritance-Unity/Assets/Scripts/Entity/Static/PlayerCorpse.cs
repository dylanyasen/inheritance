﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCorpse : StaticEntity
{
    public List<int> itemIDs;

    void Awake()
    {
        base.Awake();
        itemIDs = new List<int>();
    }

    public void Load()
    {
        m_trans.position = GM.instance._serilizationManager.LoadCorpsePos();
        itemIDs = GM.instance._serilizationManager.LoadCorpseItem();
    }

    public void Save()
    {
        GM.instance._serilizationManager.SaveCorpseItem(itemIDs);
        GM.instance._serilizationManager.SaveCorpsePos(GetPos());
    }

    public void SetItems(List<int> items)
    {
        itemIDs = items;
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
    }

    public override void Interact()
    {
        base.Interact();

        foreach (int id in itemIDs)
            GM.instance._player.AddItemToInventory(id);


        Destroy(gameObject);
    }
}
