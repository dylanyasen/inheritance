﻿using UnityEngine;
using System.Collections;

public class GemButton : StaticEntity
{
    public GameObject triggerObj;

    public bool activated = false;

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);

        if (triggerObj != null && !activated)
        {
            activated = true;
        }
    }

    private void Trigger()
    {
        //temp
        triggerObj.SetActive(false);
    }
}
