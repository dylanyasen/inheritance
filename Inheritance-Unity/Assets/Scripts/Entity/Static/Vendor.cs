﻿using UnityEngine;
using System.Collections;

public class Vendor : StaticEntity
{
    public int[] itemID;
    public int cost = 100;

    public override void Interact()
    {
        Player p = (Player)triggeredEntity;

        // return false if not enough gold
        if (!p.SpendGold(cost))
            return;

        base.Interact();

        VendItem();
    }

    private void VendItem()
    {
        int randomIndex = Random.Range(0, itemID.Length);

        int randomID = itemID[randomIndex];

        GameObject pickup = Instantiate(Resources.Load<GameObject>(Constants.EntityPrefabPath + "Pickup"), transform.position, Quaternion.identity) as GameObject;
        pickup.GetComponent<Pickup>().Init(randomID);
    }
}
