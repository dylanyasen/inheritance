﻿using UnityEngine;
using System.Collections;

public abstract class StaticEntity : Entity
{
    // stationary objects
    public GameObject triggeredObj { get; protected set; }
    public Entity triggeredEntity { get; protected set; }
    public string triggeredObjTag { get; protected set; }
    public LayerMask triggeredObjLayer { get; protected set; }

    public Player collidedEntity { get; protected set; }

    public Collider2D m_collider { get; private set; }

    public bool canInteract { get; protected set; }

    public Transform[] spawnPoint;

    protected override void Awake()
    {
        base.Awake();

        m_collider = GetComponent<Collider2D>();

        canInteract = true;

        // randomize spawn position
        // if spawn points given
        if (spawnPoint.Length >= 1)
            SetPos(spawnPoint[Random.Range(0, spawnPoint.Length)].position);
    }

    protected virtual void OnCollisionEnter2D(Collision2D other)
    {
        if (canInteract && other.gameObject.tag == "Player")
        {
            Debug.Log("collided " + gameObject.name + " " + other.gameObject.name);
            collidedEntity = other.gameObject.GetComponent<Player>();

            Debug.Log(collidedEntity.name);
            Interact();
            canInteract = false;


        }
    }

    protected virtual void OnCollisionStay2D(Collision2D other)
    {

    }

    protected virtual void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            canInteract = true;
            collidedEntity = null;
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (canInteract && other.gameObject.tag == "Player")
        {
            Debug.Log("triggered " + gameObject.name + " " + other.gameObject.name);

            triggeredEntity = other.gameObject.GetComponent<Player>();
            Interact();
            canInteract = false;

        }

    }

    protected virtual void OnTriggerStay2D(Collider2D other)
    {
    }

    protected virtual void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            triggeredEntity = null;
            canInteract = true;
        }
    }

    public virtual void Interact()
    {
        if (m_anim != null)
            m_anim.SetTrigger("trigger");
        Debug.Log(gameObject.name + " interact");
    }
}
