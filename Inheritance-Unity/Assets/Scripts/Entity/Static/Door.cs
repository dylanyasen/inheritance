﻿using UnityEngine;
using System.Collections;

public class Door : StaticEntity
{
    public enum DoorType
    {
        entry,
        exit
    }

    public DoorType type;


    public bool isBossRoomDoor;
    public bool isActive { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        isActive = true;
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);

        if (other.tag == "Player")
        {
            if (!isActive)
                return;

            if (type == DoorType.exit)
                GM.instance._levelManager.GoToNextLevel();
            else
                GM.instance._levelManager.GoToPreviousLevel();

            Debug.Log("enter");
        }
    }

    protected override void OnTriggerExit2D(Collider2D other)
    {
        if (!isActive && !isBossRoomDoor)
            isActive = true;

        base.OnTriggerExit2D(other);
    }

    public void SetColliderActive(bool b = true)
    {
        isActive = b;
    }

    public override Vector2 GetPos()
    {
        Vector2 pos = m_trans.position;
        pos.y -= 100;
        return pos;
    }
}
