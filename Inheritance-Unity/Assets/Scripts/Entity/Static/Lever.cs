﻿using UnityEngine;
using System.Collections;

public class Lever : StaticEntity
{
    public GameObject TriggerObject;
    public bool isSwitchOn { get; private set; }

  

    protected override void Awake()
    {
        base.Awake();

    }

    public override void Interact()
    {
        base.Interact();

        isSwitchOn = !isSwitchOn;

        TriggerObject.SetActive(!isSwitchOn);
    }
}
