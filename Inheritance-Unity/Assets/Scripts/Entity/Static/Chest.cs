﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Chest : StaticEntity
{
    public List<Item> items;

    public int MAX_ITEM_AMT = 4;
    public int MIN_ITEM_AMT = 2;


    protected override void Awake()
    {
        base.Awake();

        // init
        items = new List<Item>();


    }

    void Start()
    {
        InitItems();
    }

    // TODO: randomize properly
    private void InitItems()
    {
        // amount of items in chest
        int itemAmt = 0;
        itemAmt = Random.Range(MIN_ITEM_AMT, MAX_ITEM_AMT);

        // total amount of items in database
        int dataAmt = ItemDatabase.instance.GetItemAmtTotal();

        for (int i = 0; i < itemAmt; i++)
        {
            // add in random items
            items.Add(ItemDatabase.instance.GetRandomItem());
        }
    }

    public override void Interact()
    {
        base.Interact();

        // float text
        string floatText = "";

        /*
         * can add checking here
         * for example : key / special requirement
         * 
        */

        Player p = (Player)triggeredEntity;

        // give items to player
        for (int i = 0; i < items.Count; i++)
        {
            p.AddItemToInventory(items[i]);
            floatText += items[i].itemName + "\n";
        }

        // clear item list
        items.Clear();

        canInteract = false;

        // TODO: Post-Effect
        // float-text, animation, particle etc

        // float text
        GM.instance.FloatText(floatText, Camera.main.WorldToScreenPoint(m_trans.position), Color.black);
    }
}
