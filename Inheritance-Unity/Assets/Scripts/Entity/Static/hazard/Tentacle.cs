﻿using UnityEngine;
using System.Collections;

public class Tentacle : StaticEntity
{
    public bool isTriggering { get; private set; }
    public bool canDoDmg { get; private set; }
    public bool selfDestory { get; private set; }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        triggeredEntity = other.gameObject.GetComponent<Player>();

        if (canInteract && other.gameObject.tag == "Player")
        {
            Interact();
            canInteract = false;
        }

        // has been triggered
        else
        {
            if (canDoDmg && triggeredEntity != null)
                triggeredEntity.ApplyDamage(m_stats.cur_dmg);
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D other)
    {
        if (!isTriggering)
        {
            triggeredEntity = null;
            canInteract = true;
        }
    }

    public override void Interact()
    {
        base.Interact();
        isTriggering = true;
    }

    public void TriggerEnd()
    {
        isTriggering = false;
    }

    public void SwitchCanDoDmg()
    {
        canDoDmg = !canDoDmg;
    }

    public void SetSelfDestory(bool b)
    {
        m_anim.SetBool("selfDestory", b);
    }

}
