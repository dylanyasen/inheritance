﻿using UnityEngine;
using System.Collections;

public class FireWall : StaticEntity
{
    protected override void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            collidedEntity = other.gameObject.GetComponent<Player>();
        }
    }

    protected override void OnCollisionStay2D(Collision2D other)
    {
        if (collidedEntity != null)
        {
            if (!collidedEntity.m_controller.m_isHurt)
                collidedEntity.ApplyDamage(m_stats.cur_dmg);
        }
    }

    protected override void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
            collidedEntity = null;
    }
}
