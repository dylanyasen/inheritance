﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    private Transform m_trans;

    public bool isOccupied; //{ get; private set; }

    public Bounds bound { get; private set; }

    public Vector2 size { get; private set; }

    void Awake()
    {
        m_trans = GetComponent<Transform>();
        gameObject.tag = "Tile";

        size = new Vector2(100, 100);
        bound = new Bounds(m_trans.position, size);
    }

    public Vector2 GetPos()
    {
        return m_trans.position;
    }



    void Update()
    {
        //if (bound.Contains(Player.instance.GetPos()))
        //    isOccupied = true;
        //else
        //    isOccupied = false;
    }


    // for debug
    public Color GetColor()
    {
        return isOccupied ? Color.red : Color.white;
    }

    public Vector2 GetSize()
    {
        return size;
    }
}
