﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager : MonoBehaviour
{
    public bool leftArrowPressed { get; private set; }
    public bool rightArrowPressed { get; private set; }
    public bool upArrowPressed { get; private set; }
    public bool downArrowPressed { get; private set; }

    public bool attackButtonDown { get; private set; }
    public bool backpackButtonDown { get; private set; }
    public bool quickUseButtionDown { get; private set; }

    public KeyCode latestKey;
    public int stackCount;

    Stack<KeyCode> inputStack;

    void Awake()
    {
        inputStack = new Stack<KeyCode>();
    }

    void Update()
    {
        MovementKey();
    }

    void MovementKey()
    {
        // pressed key state
        leftArrowPressed = Input.GetKey(KeyCode.LeftArrow);
        rightArrowPressed = Input.GetKey(KeyCode.RightArrow);
        upArrowPressed = Input.GetKey(KeyCode.UpArrow);
        downArrowPressed = Input.GetKey(KeyCode.DownArrow);

        // down key state
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            if (inputStack.Count == 0 || inputStack.Peek() != KeyCode.LeftArrow)
                inputStack.Push(KeyCode.LeftArrow);

        if (Input.GetKeyDown(KeyCode.RightArrow))
            if (inputStack.Count == 0 || inputStack.Peek() != KeyCode.RightArrow)
                inputStack.Push(KeyCode.RightArrow);

        if (Input.GetKeyDown(KeyCode.UpArrow))
            if (inputStack.Count == 0 || inputStack.Peek() != KeyCode.UpArrow)
                inputStack.Push(KeyCode.UpArrow);

        if (Input.GetKeyDown(KeyCode.DownArrow))
            if (inputStack.Count == 0 || inputStack.Peek() != KeyCode.DownArrow)
                inputStack.Push(KeyCode.DownArrow);

        // up key state
        if (Input.GetKeyUp(KeyCode.LeftArrow))
            if (inputStack.Peek() == KeyCode.LeftArrow)
                inputStack.Pop();

        if (Input.GetKeyUp(KeyCode.RightArrow))
            if (inputStack.Peek() == KeyCode.RightArrow)
                inputStack.Pop();

        if (Input.GetKeyUp(KeyCode.UpArrow))
            if (inputStack.Peek() == KeyCode.UpArrow)
                inputStack.Pop();

        if (Input.GetKeyUp(KeyCode.DownArrow))
            if (inputStack.Peek() == KeyCode.DownArrow)
                inputStack.Pop();

        // pop remaining trash key
        if (inputStack.Count > 0 && !Input.GetKey(inputStack.Peek()))
            inputStack.Pop();

        // get latest key
        if (inputStack.Count > 0)
            latestKey = inputStack.Peek();
        else
            latestKey = KeyCode.None;   

        stackCount = inputStack.Count;
    }

    public void PushKey(KeyCode key)
    {
        inputStack.Push(key);
    }

}
