﻿using UnityEngine;
using System.Collections;

public class GM : MonoBehaviour
{
    public static GM instance;

    public SceneManager _sceneManager { get; private set; }
    public LevelManager _levelManager { get; private set; }
    public SerilizationManager _serilizationManager { get; private set; }
    public InputManager _inputManager { get; private set; }
    public InheritanceManager _inheritanceManager { get; private set; }
    public BGMManager _bgmManager { get; private set; }

    public Player _player { get; private set; }
    public CameraController _camController { get; private set; }

    public bool canControl { get; private set; }

    public GameObject floatText { get; private set; }

    public PlayerStatGui ingame_gui { get; private set; }

    void Awake()
    {
        // be careful with super-lazy singleton
        instance = this;
        DontDestroyOnLoad(gameObject);

        _sceneManager = GetComponent<SceneManager>();
        _levelManager = GetComponent<LevelManager>();
        _serilizationManager = GetComponent<SerilizationManager>();
        _inputManager = GetComponent<InputManager>();
        _inheritanceManager = GetComponent<InheritanceManager>();
        _bgmManager = GetComponent<BGMManager>();

        // move to loader
        floatText = Resources.Load<GameObject>(Constants.GuiPrefabsPath + "FloatText");
    }

    void Start()
    {
        //_serilizationManager.ClearPlayer();
        _serilizationManager.ClearAll();
        // ConstantManager.instance.mainCam.orthographicSize = Screen.height / 2 * ConstantManager.instance.pixelPerUnit * 2f;
    }

    void OnLevelWasLoaded(int level)
    {
        // TODO: careful the sceneIndex

        // theme loading scene
        // **Attention: *scene index* might change during development
        if (level == 1)
        {
            // this is set by SetTheme_GUI_Button Script
            _sceneManager.AsyncTransitToScene(_levelManager.levelName);
        }

        // enter first game level of any theme
        else if (level == 3 || level == 4) // glowing mushroom or forest
        {
            _levelManager.EnterLevel();

            // init player,camera,etc
            Init();

            // play bgm
            _bgmManager.PlayBGM();
        }
        else
        {
            _bgmManager.StopBGM();
        }
    }

    void Init()
    {
        Level curLvl = _levelManager.GetCurrentLevel();

        GameObject i = Instantiate(Resources.Load(Constants.InventoryPrefabsPath + "InventoryCanvas"), new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        InventoryPanel ip = i.transform.GetChild(0).GetComponent<InventoryPanel>();

        string className = _serilizationManager.LoadPlayer();
        // in-game-GUI
        GameObject inGui;
        if (className == Constants.Heretic)
            inGui = Instantiate(Resources.Load(Constants.GuiPrefabsPath + "In-Game-GUI-Canvas-Special"), new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        else
            inGui = Instantiate(Resources.Load(Constants.GuiPrefabsPath + "In-Game-GUI-Canvas"), new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        ingame_gui = inGui.GetComponent<PlayerStatGui>();

        //GameObject p = Instantiate(Resources.Load(Constants.EntityPrefabPath + "Player"), curLvl.GetDoorOffsetPos(Door.DoorType.entry), Quaternion.identity) as GameObject;

        Debug.Log(className);
        GameObject p = Instantiate(Resources.Load(Constants.PlayerPrefabPath + className), curLvl.GetDoorOffsetPos(Door.DoorType.entry), Quaternion.identity) as GameObject;

        _player = p.GetComponent<Player>();
        _player.m_controller.SetCanControl(true);
        _player.SetStatsGUI(ingame_gui);

        GameObject c = Instantiate(Resources.Load(Constants.CameraPrefabsPath + "FollowCam"), new Vector3(p.transform.position.x, p.transform.position.y, -10), Quaternion.identity) as GameObject;
        _camController = c.GetComponent<CameraController>();
        _camController.SetFollow(_player.m_trans);
    }

    void Update()
    {

        // =================== Debug ====================//

        if (Input.GetKeyDown(KeyCode.F1))
        {
            _levelManager.GoToNextLevel();
        }

        else if (Input.GetKeyDown(KeyCode.F2))
        {
            _sceneManager.TransitToScene("Menu");
        }




        // testing pop up text
        if (Input.GetKeyDown(KeyCode.T))
        {
            GameObject g = Instantiate(floatText, transform.position, Quaternion.identity) as GameObject;
            FloatText f = g.GetComponent<FloatText>();
            f.Pop("test");
        }
    }

    public void GameOver()
    {
        // disable objects
        BlackScreenMask.instance.gameObject.SetActive(false);
        _levelManager.GetCurrentLevel().transform.parent.gameObject.SetActive(false);
        ingame_gui.gameObject.SetActive(false);

        // death gui
        GameObject deathOver = Instantiate(Resources.Load(Constants.GuiPrefabsPath + "Death-Overlay-Canvas"), _player.GetPos(), Quaternion.identity) as GameObject;

        // save player as parent
        // returns bool
        // if parent slots are full
        // WON'T save
        _serilizationManager.SaveParentToEmptySlot(_player.name);

        // clear 
        _levelManager.Clear();

        Debug.Log(_serilizationManager.LoadParents());
    }

    public void PickSpouse()
    {
        // get parent
        string parent1 = _serilizationManager.LoadParent(0);
        string parent2 = _serilizationManager.LoadParent(1);

        //string parents[]

        // 1=>if has one parent
        // don't need to handle
        // go to class selection directly


        // =>has two parents ==> second generation
        // randomly DELETE one parent 
        if (parent1 != "NULL" && parent2 != "NULL")
        {
            int rand = Random.Range(0, 2);
            _serilizationManager.SaveParent(rand, "NULL");
        }

        _sceneManager.TransitToScene("ClassSelection");
    }

    public void PickChild()
    {
        // get parents
        string parent1 = _serilizationManager.LoadParent(0);
        string parent2 = _serilizationManager.LoadParent(1);

        // get new player from combnation dictionary
        string player = _inheritanceManager.GetCombination(parent1, parent2);

        _serilizationManager.SavePlayer(player);

        // temp
        _sceneManager.TransitToScene("LevelSelection");
    }

    public void FloatText(string txt, Vector2 pos, Color color)
    {
        GameObject g = Instantiate(floatText, pos, Quaternion.identity) as GameObject;
        FloatText f = g.GetComponent<FloatText>();
        f.Pop(txt, color);
    }
}
