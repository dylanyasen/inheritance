﻿using UnityEngine;
using System.Collections;

public class ArmorManager : MonoBehaviour
{
    public Transform Helmet_AttachPoint;
    public Transform Cloth_AttachPoint;
    public Transform Shoulder_L_AttachPoint;
    public Transform Shoulder_R_AttachPoint;
    public Transform Sleeve_L_AttachPoint;
    public Transform Sleeve_R_AttachPoint;

    private Player _player;

    private SpriteRenderer Helmet_AttachPoint_Renderer;
    private SpriteRenderer Cloth_AttachPoint_Renderer;
    private SpriteRenderer Shoulder_AttachPoint_L_Renderer;
    private SpriteRenderer Shoulder_AttachPoint_R_Renderer;
    private SpriteRenderer Sleeve_AttachPoint_L_Renderer;
    private SpriteRenderer Sleeve_AttachPoint_R_Renderer;

    public Helmet currentHelmet;
    public Cloth currentCloth;

    void Awake()
    {
        Helmet_AttachPoint_Renderer = Helmet_AttachPoint.GetComponent<SpriteRenderer>();
        Cloth_AttachPoint_Renderer = Cloth_AttachPoint.GetComponent<SpriteRenderer>();

        Shoulder_AttachPoint_L_Renderer = Shoulder_L_AttachPoint.GetComponent<SpriteRenderer>();
        Shoulder_AttachPoint_R_Renderer = Shoulder_R_AttachPoint.GetComponent<SpriteRenderer>();

        Sleeve_AttachPoint_L_Renderer = Sleeve_L_AttachPoint.GetComponent<SpriteRenderer>();
        Sleeve_AttachPoint_R_Renderer = Sleeve_R_AttachPoint.GetComponent<SpriteRenderer>();

        _player = GetComponent<Player>();
    }

    public void Equip(Armor armor)
    {
        //if (armor.itemSprite == null)
        //{
        //    Debug.Log("armor sprite null");
        //    return;
        //}

        if (armor is Helmet)
        {
            Helmet_AttachPoint_Renderer.sprite = armor.sprites[1];
            currentHelmet = (Helmet)armor;
        }


        else if (armor is Cloth)
        {
            Cloth cloth = (Cloth)armor;

            Cloth_AttachPoint_Renderer.sprite = armor.sprites[1];

            Shoulder_AttachPoint_L_Renderer.sprite = cloth.shoulderSprite;
            Shoulder_AttachPoint_R_Renderer.sprite = cloth.shoulderSprite;


            Sleeve_AttachPoint_L_Renderer.sprite = cloth.sleeveSprite;
            Sleeve_AttachPoint_R_Renderer.sprite = cloth.sleeveSprite;

            currentCloth = (Cloth)armor;
        }

        SetDEF(armor._def);
    }

    public void UnEquip(Armor armor)
    {
        Debug.Log("unequip: " + armor.itemName);


        if (armor is Helmet)
        {
            Helmet_AttachPoint_Renderer.sprite = null;
            currentHelmet = null;
        }

        else if (armor is Cloth)
        {
            Cloth cloth = (Cloth)armor;

            Cloth_AttachPoint_Renderer.sprite = null;

            Shoulder_AttachPoint_L_Renderer.sprite = null;
            Shoulder_AttachPoint_R_Renderer.sprite = null;

            Sleeve_AttachPoint_L_Renderer.sprite = null;
            Sleeve_AttachPoint_R_Renderer.sprite = null;

            currentCloth = null;
        }

        SetDEF(-armor._def);
    }

    private void SetDEF(int def)
    {
        _player.m_stats.SetExtraDEF(def);
    }

    public void SwitchSprite(int dir)
    {
        if (currentCloth != null)
            if (currentCloth.sprites[dir] != null)
                Cloth_AttachPoint_Renderer.sprite = currentCloth.sprites[dir];

        if (currentHelmet != null)
            if (currentHelmet.sprites[dir] != null)
                Helmet_AttachPoint_Renderer.sprite = currentHelmet.sprites[dir];
    }
}
