﻿using UnityEngine;
using System.Collections;

public class WeaponManager : MonoBehaviour
{
    //public float _dmg { get; private set; }
    private Player _player;

    // private float defaultDMG

    // TODO: add other armor attach points here ==> maybe in another gear manager
    public Transform WeaponPoint_L; //{ get; private set; }
    public Transform WeaponPoint_R; //{ get; private set; }
    private Transform attachPoint;
    //public Sprite fist_sprite;

    private SpriteRenderer WeaponPoint_L_Renderer;
    private SpriteRenderer WeaponPoint_R_Renderer;

    public Weapon currentWeapon { get; private set; }

    void Awake()
    {
        if (WeaponPoint_L)
            WeaponPoint_L_Renderer = WeaponPoint_L.GetComponent<SpriteRenderer>();

        if (WeaponPoint_R)
            WeaponPoint_R_Renderer = WeaponPoint_R.GetComponent<SpriteRenderer>();

        _player = GetComponent<Player>();

    }

    void Start()
    {
        // default fist sprite
        //WeaponPoint_L_Renderer.sprite = fist_sprite;
    }

    public void Equip(Weapon weapon)
    {
        if (weapon.itemSprite == null)
        {
            Debug.Log("equipment sprite null");
            return;
        }

        Sprite equipmentSprite = weapon.itemSprite;

        // render the item sprite at attach point
        WeaponPoint_L_Renderer.sprite = equipmentSprite;

        currentWeapon = weapon;

        SetDMG(weapon._dmg);

        _player.m_controller.SetArmed(true);
        _player.m_controller.SetSwing(weapon.isSwing);
    }

    public void UnEquip(Weapon weapon)
    {
        WeaponPoint_L_Renderer.sprite = null;

        // remove damage buff
        SetDMG(-weapon._dmg);

        currentWeapon = null;

        _player.m_controller.SetArmed(false);
    }

    // ================== Accessors ======================= //

    private void SetDMG(int dmg)
    {
        //this._dmg = dmg;
        _player.m_stats.AddExtraDMG(dmg);
    }

}