﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InheritanceManager : MonoBehaviour
{
    Dictionary<string, string> inheritanceDic;

    void Awake()
    {
        inheritanceDic = new Dictionary<string, string>();

        inheritanceDic.Add(Constants.Barbarian + Constants.Ranger, Constants.BarbarianRanger);
        inheritanceDic.Add(Constants.Ranger + Constants.Barbarian, Constants.BarbarianRanger);

        inheritanceDic.Add(Constants.Barbarian + Constants.Oracle, Constants.BarbarianOracle);
        inheritanceDic.Add(Constants.Oracle + Constants.Barbarian, Constants.BarbarianOracle);

        inheritanceDic.Add(Constants.Barbarian + Constants.Magician, Constants.BarbarianMagician);
        inheritanceDic.Add(Constants.Magician + Constants.Barbarian, Constants.BarbarianMagician);

        inheritanceDic.Add(Constants.Ranger + Constants.Oracle, Constants.RangerOracle);
        inheritanceDic.Add(Constants.Oracle + Constants.Ranger, Constants.RangerOracle);

        inheritanceDic.Add(Constants.Ranger + Constants.Magician, Constants.RangerMagician);
        inheritanceDic.Add(Constants.Magician + Constants.Ranger, Constants.RangerMagician);

        inheritanceDic.Add(Constants.Oracle + Constants.Magician, Constants.OracleMagician);
        inheritanceDic.Add(Constants.Magician + Constants.Oracle, Constants.OracleMagician);
    }

    public string GetCombination(string parent1, string parent2)
    {
        if (parent1 == parent2)
            return Constants.Wastrel;

        else
            return inheritanceDic[parent1 + parent2];
    }
}
