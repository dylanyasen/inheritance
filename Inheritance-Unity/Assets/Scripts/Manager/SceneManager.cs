﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class SceneManager : MonoBehaviour
{
    // temp
    private Text loadingText;
    private LoadingBar_GUI loadingBar;

    void Start()
    {

    }

    public void TransitToScene(string name)
    {
        Application.LoadLevel(name);
    }

    // *only used when loading in  to new theme
    public void AsyncTransitToScene(string name)
    {
        //loadingText = GameObject.Find("loadingText").GetComponent<Text>();

        GameObject obj = Resources.Load<GameObject>(Constants.GuiPrefabsPath + "LoadingBarCanvas-" + name);
        GameObject g = Instantiate(obj, Vector2.zero, Quaternion.identity) as GameObject;
        loadingBar = g.GetComponent<LoadingBar_GUI>();

        // temp
        // loading is too fast
        //StartCoroutine(Wait(2f, name));

        StartCoroutine(AsyncLoad(name));
    }

    IEnumerator AsyncLoad(string level)
    {
        Debug.Log("loading");

        //loadingText.text = "loading...";

        yield return new WaitForSeconds(1.0f);

        AsyncOperation async = Application.LoadLevelAsync(level);

        while (!async.isDone)
        {
            //loadingText.text = async.progress.ToString();
            loadingBar.bar.fillAmount = async.progress;
            //Debug.Log(async.progress);
            yield return null;
        }

        //Debug.Log("Loading complete");
    }
}
