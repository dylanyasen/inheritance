﻿using UnityEngine;
using System.Collections;
using System.Text;

public class BGMManager : MonoBehaviour
{
    public AudioClip bgmClip; //{ get; private set; }

    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayBGM()
    {
        audioSource.clip = bgmClip;
        audioSource.Play();
    }

    public void StopBGM()
    {
        audioSource.Stop();
    }

    public void LoadMusic(string path)
    {
        bgmClip = Resources.Load<AudioClip>(path);
        //bgmClip = Resources.Load<AudioClip>(folderPath);
    }

    public void SetBGM(string level)
    {
        //StringBuilder sb = new StringBuilder();
        string path = Constants.bgmFolder + level + "/bgm";

        LoadMusic(path);
    }
}
