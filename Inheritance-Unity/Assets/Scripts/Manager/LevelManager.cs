﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// TODO: clear everything with game over
public class LevelManager : MonoBehaviour
{
    //public TileMapBuilder.MapTheme theme;
    // levels
    //public string[] mushroomLevelNames = { "GlowingDungeon_Level5_BossRoom", "GlowingDungeon_Level4", "GlowingDungeon_Level3", "GlowingDungeon_Level2", "GlowingDungeon_Level1" };

    public enum LevelTheme
    {
        GlowingMushroom,
        Forest
    }

    public Stack<Level> levels = new Stack<Level>();
    public List<Level> popedLevels = new List<Level>();

    public LevelTheme theme { get; private set; }
    public string levelName { get; private set; }

    public void EnterLevel()
    {
        Debug.Log("enter level");

        Transform levelObjTrans = GameObject.Find("levels").transform;

        // reversely push levels to stack 
        for (int i = levelObjTrans.childCount - 1; i >= 0; i--)
            levels.Push(levelObjTrans.GetChild(i).GetComponent<Level>());

        // get corpse 
        if (GM.instance._serilizationManager.HasCorpsePos())
        {
            Vector2 pos = GM.instance._serilizationManager.LoadCorpsePos();
            GameObject g = Instantiate(Resources.Load<GameObject>(Constants.EntityPrefabPath + "PlayerCorpse"), Vector2.zero, Quaternion.identity) as GameObject;
            PlayerCorpse corpse = g.GetComponent<PlayerCorpse>();
            corpse.SetPos(pos);
            corpse.SetItems(GM.instance._serilizationManager.LoadCorpseItem());
        }

        // show top level
        levels.Peek().Show(true);

        // spawn enemy
        levels.Peek().SpawnEnemy();
    }

    // TODO: Error when go back from boss level
    // since entry and exit is the same door
    // ****************************************
    public void GoToNextLevel()
    {
        BlackScreenMask.instance.Show(0);

        // pop current level
        Level lvl = levels.Pop();

        // hide enemy of current level
        // lvl.HideEnemy();
        // * don't have to because enemy is child of level. disable level will disable all

        // hide poped level
        lvl.Show(false);

        // append poped level to list
        popedLevels.Add(lvl);

        //// show new level on top
        //levels.Peek().Show(true);

        //// spawn enemy
        //levels.Peek().SpawnEnemy();

        // get the door of next level
        Door door = GetCurrentLevel().GetDoor(Door.DoorType.entry);

        // disable door collider
        door.SetColliderActive(false);

        // get door's position
        Vector2 pos = door.GetPos();

        // move player and camera to door's position
        Player player = GM.instance._player;
        player.SetPos(pos);
        player.m_controller.SetCanControl(false);
        //player.m_controller.TurnTo(DynamicEntityController.DIR.DOWN);
        player.m_controller.SetFaceTo(0, -1);
        GM.instance._camController.GoToPos(pos);
    }

    public void InitLevel()
    {
        // show new level on top
        levels.Peek().Show(true);

        // spawn enemy
        levels.Peek().SpawnEnemy();
    }


    public void GoToPreviousLevel()
    {
        // if is first level
        // which means poped lvl list is empty
        if (popedLevels.Count < 1)
        {
            Debug.Log("this is the first level. can't go back");
            return;
        }

        BlackScreenMask.instance.Show(1);

        // hide current level
        levels.Peek().Show(false);

        // hide enemy in current level
        // * don't have to because enemy is child of level. disable level will disable all
        //levels.Peek().HideEnemy();

        // push back last poped level to stack
        levels.Push(popedLevels[popedLevels.Count - 1]);

        // remove pushed level from poped list 
        popedLevels.RemoveAt(popedLevels.Count - 1);

        //// show new level
        //levels.Peek().Show(true);

        //// show enemy in new level
        //levels.Peek().SpawnEnemy();

        // get the door of next level
        Door door = GetCurrentLevel().GetDoor(Door.DoorType.exit);

        // disable door collider
        door.SetColliderActive(false);

        // get door position
        Vector2 pos = door.GetPos();

        // move camera and player to door's position
        Player player = GM.instance._player;
        player.SetPos(pos);
        //player.m_controller.SetCanControl(false);
        //player.m_controller.TurnTo(DynamicEntityController.DIR.DOWN);
        player.m_controller.SetCanControl(false);
        player.m_controller.SetFaceTo(0, -1);
        GM.instance._camController.GoToPos(pos);
    }

    /// <summary>
    /// clean up stacks and data
    /// </summary>
    public void Clear()
    {
        levels.Clear();
        popedLevels.Clear();
    }

    /// <summary>
    /// 
    /// </summary>
    public Level GetCurrentLevel()
    {
        return levels.Peek();
    }

    public void SetTheme(LevelTheme t)
    {
        this.theme = t;

        switch ((int)t)
        {
            case 0:
                SetLevel(Constants.GlowingMushroomScene);
                GM.instance._bgmManager.SetBGM(Constants.GlowingMushroomScene);
                break;

            case 1:
                SetLevel(Constants.ForestScene);
                GM.instance._bgmManager.SetBGM(Constants.ForestScene);
                break;
        }
    }

    private void SetLevel(string name)
    {
        levelName = name;
    }

}