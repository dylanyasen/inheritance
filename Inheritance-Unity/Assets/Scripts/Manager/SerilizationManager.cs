﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SerilizationManager : MonoBehaviour
{
    /// <summary>
    /// Save & Load Player Corpse Data
    /// </summary>
    public void SaveCorpsePos(Vector2 pos)
    {
        PlayerPrefs.SetFloat("corpse_x", pos.x);
        PlayerPrefs.SetFloat("corpse_y", pos.y);
    }

    public void SaveCorpseItem(List<int> itemID)
    {
        // TODO: SPLIT ID STRING BY ','
        string itemIDasStr = "";

        for (int i = 0; i < itemID.Count; i++)
            itemIDasStr += itemID[i] + ",";

        PlayerPrefs.SetString("corpse_items", itemIDasStr);

        Debug.Log("saved");
    }

    public List<int> LoadCorpseItem()
    {
        if (!PlayerPrefs.HasKey("corpse_items"))
            return null;

        string itemIDasStr = PlayerPrefs.GetString("corpse_items");

        string[] IDs = itemIDasStr.Split(new char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);

        List<int> itemIDs = new List<int>();


        for (int i = 0; i < IDs.Length; i++)
            itemIDs.Add(int.Parse(IDs[i].ToString()));

        //string s = "1234";
        //Debug.Log(int.Parse(s[1].ToString()));

        Debug.Log("loaded");
        Debug.Log(itemIDs.Count);

        return itemIDs;
    }

    public bool HasCorpsePos()
    {
        return (PlayerPrefs.HasKey("corpse_x") && PlayerPrefs.HasKey("corpse_y"));
    }

    public Vector2 LoadCorpsePos()
    {
        Vector2 pos;

        pos.x = PlayerPrefs.GetFloat("corpse_x");
        pos.y = PlayerPrefs.GetFloat("corpse_y");

        return pos;
    }

    public void ClearCorpsePos()
    {
        PlayerPrefs.DeleteKey("corpse_x");
        PlayerPrefs.DeleteKey("corpse_y");
    }

    public void SavePos(Transform trans)
    {
        PlayerPrefs.SetFloat(trans.name + "_x", trans.position.x);
        PlayerPrefs.SetFloat(trans.name + "_y", trans.position.y);
    }

    // ======== Inheritance =========== //

    public bool SaveParentToEmptySlot(string className)
    {
        // save player
        if (LoadParent(0) == "NULL")
        {
            SaveParent(0, className);
            return true;
        }
        else if (LoadParent(1) == "NULL")
        {
            SaveParent(1, className);
            return true;
        }

        return false;
    }

    public void SaveParent(int index, string className)
    {
        string parent = "parent" + index;

        PlayerPrefs.SetString(parent, className);
    }

    public string LoadParent(int index)
    {
        string parent = "parent" + index;

        if (PlayerPrefs.HasKey(parent))
            return PlayerPrefs.GetString(parent);

        else
            return "NULL";
    }

    public string[] LoadParents()
    {
        string p1 = LoadParent(0);
        string p2 = LoadParent(1);

        string[] parents = new string[2];
        for (int i = 0; i < parents.Length; i++)
            parents[i] = LoadParent(i);

        return parents;
    }

    public void ClearParent(int index)
    {
        string parent = "parent" + index;

        if (PlayerPrefs.HasKey(parent))
            PlayerPrefs.SetString(parent, "NULL");

    }

    public void ClearParents()
    {
        string p1 = "parent0";
        string p2 = "parent1";

        if (PlayerPrefs.HasKey(p1))
            PlayerPrefs.DeleteKey(p1);

        if (PlayerPrefs.HasKey(p2))
            PlayerPrefs.DeleteKey(p2);
    }

    // =========== Class Serilization ============== //
    public void SavePlayer(string playerClass)
    {
        PlayerPrefs.SetString("player", playerClass);
    }

    public string LoadPlayer()
    {
        if (PlayerPrefs.HasKey("player"))
            return PlayerPrefs.GetString("player");
        else
            return null;
    }

    public void ClearPlayer()
    {
        PlayerPrefs.DeleteKey("player");
    }

    // ============  Danger Zone ================ //
    public void ClearAll()
    {
        PlayerPrefs.DeleteAll();
    }



}
