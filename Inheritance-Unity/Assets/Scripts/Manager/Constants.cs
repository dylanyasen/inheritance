﻿using UnityEngine;
using System.Collections;

public class Constants : MonoBehaviour
{
    // prefab path
    public const string EntityPrefabPath = "Prefabs/EntityPrefabs/";
    public const string HazardPrefabPath = "Prefabs/EntityPrefabs/Level Element/Hazard/";
    public const string EnemyPrefabPath = "Prefabs/EntityPrefabs/Enemy/";
    public const string PlayerPrefabPath = "Prefabs/EntityPrefabs/Player/";
    public const string ProjectilePrefabPath = "Prefabs/Projectile/";
    public const string SpellPrefabPath = "Prefabs/SpellPrefabs/";
    public const string FxPrefabPath = "Prefabs/FX/";


    //public const string ProjectilePrefabPath = "Prefabs/EntityPrefabs/Projectile/";
    public const string CameraPrefabsPath = "Prefabs/CameraPrefabs/";
    public const string GuiPrefabsPath = "Prefabs/GuiPrefabs/";
    public const string InventoryPrefabsPath = "Prefabs/InventoryPrefabs/";

    // scene name
    public const string GlowingMushroomScene = "GlowingMushroom";
    public const string ForestScene = "Forest";

    // BGM folder
    public const string bgmFolder = "Sound/BGM/";

    // SFX
    public const string sfxFolder = "Sound/SFX/";

    // texture path
    public const string BackGroundImagePath = "Images/Background/";

    // class name
    public const string Barbarian = "Barbarian";
    public const string Ranger = "Ranger";
    public const string Oracle = "Oracle";
    public const string Magician = "Magician";
    public const string Bard = "Bard";
    public const string Alchemist = "Alchemist";
    public const string Exorcist = "Exorcist";
    public const string Heretic = "Heretic";
    public const string Spellsword = "Spellsword";
    public const string Wastrel = "Wastrel";
    public const string Paladin = "Paladin";

    public const string BarbarianRanger = Bard;
    public const string BarbarianOracle = Paladin;
    public const string BarbarianMagician = Spellsword;
    public const string RangerOracle = Exorcist;
    public const string RangerMagician = Alchemist;
    public const string OracleMagician = Heretic;

}
