﻿using UnityEngine;
using System.Collections;

public class ConstantManager : MonoBehaviour
{
    public static ConstantManager instance;

    public Camera mainCam { get; private set; }
    public float mainCamSize { get; private set; }
    public float mainCamHeight { get; private set; }
    public float mainCamWidth { get; private set; }
    public float mainCamAspectRatio { get; private set; }

    public float pixelPerUnit { get; private set; }

    void Awake()
    {
        instance = this;

        SetUpConstant();
    }

    void SetUpConstant()
    {
        mainCam = Camera.main;
        mainCamSize = 11.8f;
        mainCam.orthographicSize = mainCamSize;
        mainCamAspectRatio = mainCam.aspect;
        mainCamHeight = mainCam.orthographicSize * 2f;
        mainCamWidth = mainCamHeight * mainCamAspectRatio;

        pixelPerUnit = 1 / 100f;
    }

}
