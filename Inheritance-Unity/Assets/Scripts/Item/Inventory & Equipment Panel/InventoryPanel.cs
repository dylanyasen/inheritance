﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

public class InventoryPanel : MonoBehaviour
{
    public static InventoryPanel instance { get; private set; }
    public Image dragItemIcon;
    public Text itemDesGUI;

    public Inventory inventory;
    public CharPanel charPanel;
    public Text goldLabel;

    public Player m_entity { get; private set; }
    public WeaponManager weaponManager { get; private set; }
    public ArmorManager armorManager { get; private set; }


    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        // ****temp******
        m_entity = Player.instance;

        weaponManager = m_entity.weaponManager;
        armorManager = m_entity.armorManager;

        //Player.instance.SetInventory(inventory, charPanel);

        Debug.Log(m_entity);
    }

    public void SwithGUI(bool b)
    {
        if (b)
        {
            // show inventory gui
            gameObject.SetActive(true);
        }
        else
        {
            // hide inventory gui
            gameObject.SetActive(false);
        }
    }


    /// <summary>
    //   return all the items(inventory & equipment)
    /// </summary>
    /// <returns></returns>
    public List<int> GetAllItems()
    {
        List<int> items = new List<int>();

        // items in inventory
        for (int i = 0; i < inventory.items.Count; i++)
        {
            int id = inventory.items[i].itemID;

            // null item
            if (id == -1)
                continue;

            items.Add(id);
        }
        // items in charpanel
        for (int i = 0; i < charPanel.items.Count; i++)
        {
            int id = charPanel.items[i].itemID;

            // null item
            if (id == -1)
                continue;

            items.Add(id);
        }

        return items;
    }

}