﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class CharPanelSlot : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    public int index { get; set; }
    public CharPanel charPanel { get; set; }
    public SpriteRenderer itemAttachPoint;

    private Inventory inventory;
    private InventoryPanel panel;
    private Image itemIcon;

    void Awake()
    {
        itemIcon = transform.GetChild(0).GetComponent<Image>();
        panel = charPanel.panel;
        inventory = panel.inventory;
    }

    void Update()
    {
        // **** this is shitty **** //
        // **** change later **** //
        if (!charPanel)
            return;

        if (ContainsItem())
        {
            itemIcon.enabled = true;
            itemIcon.sprite = charPanel.items[index].itemIcon;
        }
        else
            itemIcon.enabled = false;
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        // right click
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            // unequip
            if (ContainsItem())
            {
                // give item back to inventory
                inventory.AddItemByID(charPanel.items[index].itemID);

                UnequipInSlotItem();
            }
        }

        // left click
        if (eventData.button == PointerEventData.InputButton.Left)
        {

            // equip item
            if (inventory.isDraggingItem)
            {
                Item draggedItem = inventory.draggedItem;

                if (draggedItem is EquipableItem)
                {
                    EquipableItem equipment = (EquipableItem)draggedItem;

                    // TODO: EquipToSlot method is vague
                    if (EquipToSlot(equipment))
                    {
                        if (equipment is Weapon)
                            panel.weaponManager.Equip((Weapon)equipment);

                        else if (equipment is Armor)
                            panel.armorManager.Equip((Armor)equipment);
                    }

                    //AttachWeaponToPlayer(equipment);
                    //equipment.ActiveItemEffect ();
                }
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        // show item description
        if (ContainsItem() && !charPanel.isDraggingItem)
            charPanel.ShowItemDescription(charPanel.items[index]);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        // hide item description
        if (ContainsItem())
            charPanel.HideItemDescription();
    }

    /*
   public void OnDrag(PointerEventData eventData)
   {
       if (ContainsItem())
       {
           // display dragged item icon at mouse position
           charPanel.ShowDraggedItem(charPanel.items[index], index);

           // delete the dragged item
           charPanel.items[index] = new Item();
       }
   }
    */

    public bool EquipToSlot(EquipableItem draggedItem)
    {
        // * important
        // * slots index has the some order as 
        // * weapon equip type enum's order

        if (index == (int)draggedItem.indexInEquipmentSlot)
        {
            // slot occupied
            if (ContainsItem())
            {
                // unequip old item
                // swap old -> new item
                // give old item back to inventory
                EquipableItem tempItem = (EquipableItem)charPanel.items[index];
                tempItem.UnEquip(panel.m_entity);

                charPanel.items[index] = draggedItem;
                inventory.items[inventory.draggedItemSlotNum] = tempItem;
                //draggedItem.ActiveItemEffect();  * we are using OnEquip now
            }

            // slot empty
            else
                charPanel.items[index] = draggedItem;

            draggedItem.OnEquip(panel.m_entity);

            inventory.HideDraggedItem();

            return true;
        }

        Debug.Log("wrong slot!");
        return false;
    }

    public void UnequipInSlotItem()
    {
        if (charPanel.items[index].itemSprite != null && itemAttachPoint != null)
            itemAttachPoint.sprite = null;

        EquipableItem CastItem = (EquipableItem)charPanel.items[index];
        //CastItem.DeactivateItemEffect();

        // TODO: maybe move this to unequip as well
        //weaponManager.UnEquip(CastItem);

        if (CastItem is Weapon)
            panel.weaponManager.UnEquip((Weapon)CastItem);

        else if (CastItem is Armor)
            panel.armorManager.UnEquip((Armor)CastItem);

        CastItem.UnEquip(panel.m_entity);
        charPanel.items[index] = new Item();
    }

    public bool ContainsItem()
    {
        if (charPanel.items[index].itemName != null)
            return true;

        return false;
    }
}

/*
// TODO: this functionality should be in weaponManager
void AttachWeaponToPlayer(EquipableItem equipItem)
{
    if (charPanel.items[index].itemSprite == null)
        return;

    weaponManager.Equip(equipItem);


    /* 2015-2-12
    if (charPanel.items[index].itemSprite == null)
        return;

    // temp init * transform doesn't mean anything here. just for init
    Transform attachPoint = transform;

    Sprite itemSprite = charPanel.items[index].itemSprite;

    if (equipItem is Weapon)
    {

        Weapon weapon = (Weapon)equipItem;

        attachPoint = weaponManager.WeaponPoint_L;

        // render the item sprite at attach point
        attachPoint.GetComponent<SpriteRenderer>().sprite = itemSprite;

        // clear previous wepon collider
        if (attachPoint.GetComponent<PolygonCollider2D>() != null)
            Destroy(attachPoint.GetComponent<PolygonCollider2D>());

        // attach new collider
        attachPoint.gameObject.AddComponent<PolygonCollider2D>();

        // update weapon dmg
        weaponManager.SetDMG(weapon._dmg);
    }
    */


// old code
/*
if (charPanel.items [index].itemSprite != null && itemAttachPoint != null)
    itemAttachPoint.sprite = charPanel.items [index].itemSprite;

//add collider for weapon
if (equipItem.equipType == EquipableItem.EquipType.Weapon) {
    // clear previous wepon collider
    if (itemAttachPoint.GetComponent<PolygonCollider2D> () != null)
        Destroy (itemAttachPoint.GetComponent<PolygonCollider2D> ());

    // attach new collider
    itemAttachPoint.gameObject.AddComponent<PolygonCollider2D> ();
}
*/

/*
// TODO:
if (charPanel.items [index].itemSprite == null)
    return;

EquipableItem.EquipType type = equipItem.equipType;
Sprite itemSprite = charPanel.items [index].itemSprite;

Transform attachPoint;

// TODO: might need to deal with *left/right hand*
if (type == EquipableItem.EquipType.Weapon) {
		
    attachPoint = weaponManager.WeaponPoint_L;

    // clear previous wepon collider
    if (attachPoint.GetComponent<PolygonCollider2D> () != null)
        Destroy (attachPoint.GetComponent<PolygonCollider2D> ());

    // attach new collider
    attachPoint.gameObject.AddComponent<PolygonCollider2D> ();
}

// render the item sprite
attachPoint.GetComponent<SpriteRenderer> ().sprite = itemSprite; 


// update weapon dmg
weaponManager.SetDMG (EquipableItem);


}
    
*/

/*
     * 
     * old script in equipToSlot
    switch (index)
    {
        case 0:
            if (draggedItem.equipType == EquipableItem.EquipType.Head)
            {
                // slot not empty
                if (ContainsItem())
                {
                    Item tempItem = charPanel.items[index];
                    charPanel.items[index] = draggedItem;
                    //InventoryPanel.instance.inventory.draggedItem = tempItem;
                    InventoryPanel.instance.inventory.items[InventoryPanel.instance.inventory.draggedItemSlotNum] = tempItem;
                    // InventoryPanel.instance.inventory.ShowDraggedItem(tempItem, -1);

                    draggedItem.ActiveItemEffect();
                }

                // slot empty
                else
                    charPanel.items[index] = draggedItem;

                InventoryPanel.instance.inventory.HideDraggedItem();

                return true;
            }
            break;

        case 1:
            if (draggedItem.equipType == EquipableItem.EquipType.Body)
            {
                // slot not empty
                if (ContainsItem())
                {
                    Item tempItem = charPanel.items[index];
                    charPanel.items[index] = draggedItem;
                    //InventoryPanel.instance.inventory.draggedItem = tempItem;
                    InventoryPanel.instance.inventory.items[InventoryPanel.instance.inventory.draggedItemSlotNum] = tempItem;

                    // InventoryPanel.instance.inventory.ShowDraggedItem(tempItem, -1);
                }

                // slot empty
                else
                    charPanel.items[index] = draggedItem;

                InventoryPanel.instance.inventory.HideDraggedItem();

                return true;
            }
            break;

        case 2:
            if (draggedItem.equipType == EquipableItem.EquipType.Legs)
            {
                // slot not empty
                if (ContainsItem())
                {
                    Item tempItem = charPanel.items[index];
                    charPanel.items[index] = draggedItem;
                    //InventoryPanel.instance.inventory.draggedItem = tempItem;
                    InventoryPanel.instance.inventory.items[InventoryPanel.instance.inventory.draggedItemSlotNum] = tempItem;

                    // InventoryPanel.instance.inventory.ShowDraggedItem(tempItem, -1);
                }

                // slot empty
                else
                    charPanel.items[index] = draggedItem;

                InventoryPanel.instance.inventory.HideDraggedItem();

                return true;
            }
            break;

        case 3:
            if (draggedItem.equipType == EquipableItem.EquipType.Weapon)
            {
                // slot not empty
                if (ContainsItem())
                {
                    // add item to slot
                    Item tempItem = charPanel.items[index];
                    charPanel.items[index] = draggedItem;
                    //InventoryPanel.instance.inventory.draggedItem = tempItem;
                    InventoryPanel.instance.inventory.items[InventoryPanel.instance.inventory.draggedItemSlotNum] = tempItem;
                    // InventoryPanel.instance.inventory.ShowDraggedItem(tempItem, -1);
                }

                // slot empty
                else
                    charPanel.items[index] = draggedItem;

                InventoryPanel.instance.inventory.HideDraggedItem();

                print("testing!!!!!!!!!!!!" + " index is " + index + " weapon state is " + (int)draggedItem.equipType);

                return true;
            }
            break;

        case 4:
            if (draggedItem.equipType == EquipableItem.EquipType.Accessory)
            {
                // slot not empty
                if (ContainsItem())
                {
                    Item tempItem = charPanel.items[index];
                    charPanel.items[index] = draggedItem;
                    //InventoryPanel.instance.inventory.draggedItem = tempItem;
                    InventoryPanel.instance.inventory.items[InventoryPanel.instance.inventory.draggedItemSlotNum] = tempItem;

                    // InventoryPanel.instance.inventory.ShowDraggedItem(tempItem, -1);
                }

                // slot empty
                else
                    charPanel.items[index] = draggedItem;

                InventoryPanel.instance.inventory.HideDraggedItem();

                return true;
            }
            break;

        case 5:
            if (draggedItem.equipType == EquipableItem.EquipType.Accessory)
            {
                // slot not empty
                if (ContainsItem())
                {
                    Item tempItem = charPanel.items[index];
                    charPanel.items[index] = draggedItem;
                    //InventoryPanel.instance.inventory.draggedItem = tempItem;
                    InventoryPanel.instance.inventory.items[InventoryPanel.instance.inventory.draggedItemSlotNum] = tempItem;

                    // InventoryPanel.instance.inventory.ShowDraggedItem(tempItem, -1);
                }

                // slot empty
                else
                    charPanel.items[index] = draggedItem;

                InventoryPanel.instance.inventory.HideDraggedItem();

                return true;
            }
            break;
     */


