﻿using UnityEngine;
using System.Collections;

public class Pickup : StaticEntity
{
    public string itemName { get; private set; }
    public int itemID { get; private set; }
    public int itemAmount { get; private set; }
    public Item item { get; private set; }

    private SpriteRenderer _renderer;

    private float pullToPlayerForce = 3;

    void Awake()
    {
        base.Awake();
        _renderer = GetComponent<SpriteRenderer>();


        // to disable in gm
        // when game over
        name = "pickup";
    }

    public void Init(int id)
    {
        item = ItemDatabase.instance.GetItemByID(id);

        itemName = item.itemName;

        _renderer.sprite = item.itemIcon;
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        triggeredEntity = other.GetComponent<Entity>();
    }

    protected override void OnTriggerStay2D(Collider2D other)
    {

        transform.position = Vector2.Lerp(transform.position, other.transform.position, Time.deltaTime * pullToPlayerForce);

        if (triggeredEntity == null)
            return;

        // reach player
        if (Vector2.SqrMagnitude(m_trans.position) - Vector2.SqrMagnitude(triggeredEntity.GetPos()) <= 0.001f)
        {
            PickUp();

         

            //Die();

            Show(false);
        }

        /*
        if (other.tag == "Player")
        {
            if (itemID < 0)
            {
                Debug.Log("item id not assigned");
                return;
            }

            // add item to the Inventory.
            other.GetComponent<Player>().inventory.AddItemByID(itemID);

            // ****************
            // add effects here
            // ****************

            Destroy(gameObject);
        }
         */
    }

    public virtual void PickUp()
    {
        Player p = (Player)triggeredEntity;
        p.AddItemToInventory(item);

        GM.instance.FloatText(itemName, Camera.main.WorldToScreenPoint(p.m_trans.position), Color.magenta);
    }
}
