﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ConsumableItem : Item
{
    GameObject _particle;


    public ConsumableItem(string name)
        : base(name)
    {
        // load particle
        //_particle = Resources.Load<GameObject>(pass in path)
    }

    public abstract bool Use(Player player);

    /*
    public List<OnUseItemEffect> itemEffects = new List<OnUseItemEffect>();

    delegate void EquipEffectDelegate();
    EquipEffectDelegate itemEffectDelegate;
    
     
    public void InitEffect()
    {
        for (int i = 0; i < itemEffects.Count; i++)
        {
            itemEffectDelegate += itemEffects[i].Use;
        }
    }

    public void ActiveItemEffect()
    {
        itemEffectDelegate();
    }
    */

}
