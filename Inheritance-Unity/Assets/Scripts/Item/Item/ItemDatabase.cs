﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDatabase : MonoBehaviour
{
    public static ItemDatabase instance { get; private set; }
    public Dictionary<string, Sprite> iconSpriteSheet { get; private set; }
    public Dictionary<string, Sprite> weaponSpriteSheet { get; private set; }
    public Dictionary<string, Sprite> armorSpriteSheet { get; private set; }
    public Dictionary<string, Sprite> classPortraitSpriteSheet { get; private set; }

    private Sprite[] icons;
    private Sprite[] weaponSprites;
    private Sprite[] armorSprites;

    public List<Item> items { get; private set; }

    void Awake()
    {
        instance = this;

        items = new List<Item>();

        // init class portrait gui sprite
        Sprite[] sp = Resources.LoadAll<Sprite>("classPortrait");
        classPortraitSpriteSheet = new Dictionary<string, Sprite>();
        // store in dictionary
        for (int i = 0; i < sp.Length; i++)
            classPortraitSpriteSheet.Add(sp[i].name, sp[i]);


        // init icon spritesheet;
        icons = Resources.LoadAll<Sprite>("itemicon");
        iconSpriteSheet = new Dictionary<string, Sprite>();

        // store item icon in dictionary
        for (int i = 0; i < icons.Length; i++)
            iconSpriteSheet.Add(icons[i].name, icons[i]);

        Debug.Log("icon passed");


        // init weapon spritesheet
        weaponSprites = Resources.LoadAll<Sprite>("weaponSprite");
        weaponSpriteSheet = new Dictionary<string, Sprite>();

        // store item sprite in dictionary
        for (int i = 0; i < weaponSprites.Length; i++)
        {
            //Debug.Log(weaponSprites[i].name);
            weaponSpriteSheet.Add(weaponSprites[i].name, weaponSprites[i]);
        }

        //Debug.Log("weapon passed");

        // init armor spritesheet
        armorSprites = Resources.LoadAll<Sprite>("armorSprite");
        armorSpriteSheet = new Dictionary<string, Sprite>();

        //Debug.Log("passed");

        // store armor sprites in dictionary
        for (int i = 0; i < armorSprites.Length; i++)
            armorSpriteSheet.Add(armorSprites[i].name, armorSprites[i]);

        // items
        int itemIDCounter = -1;

        // debug
        //foreach (KeyValuePair<string, Sprite> val in armorSpriteSheet)
        //  Debug.Log(val.Key);

        Helmet helmet;
        Cloth cloth;
        MeleeWeapon meleeWeapon;
        RangeWeapon rangeWeapon;

        // id = 0
        helmet = new Helmet("Oracle's Hood", 1);
        helmet.itemID = ++itemIDCounter;
        helmet.itemAmount = 1;
        items.Add(helmet);

        // id = 1
        cloth = new Cloth("Oracle's Robes", 1);
        cloth.itemID = ++itemIDCounter;
        cloth.itemAmount = 1;
        items.Add(cloth);

        // id = 2
        helmet = new Helmet("Ranger's Hood", 1);
        helmet.itemID = ++itemIDCounter;
        helmet.itemAmount = 1;
        items.Add(helmet);

        // id = 3
        cloth = new Cloth("Ranger's Outfit", 1);
        cloth.itemID = ++itemIDCounter;
        cloth.itemAmount = 1;
        items.Add(cloth);

        // id = 4
        helmet = new Helmet("Heretic's Hood", 1);
        helmet.itemID = ++itemIDCounter;
        helmet.itemAmount = 1;
        items.Add(helmet);

        // id = 5
        cloth = new Cloth("Heretic's Robes", 1);
        cloth.itemID = ++itemIDCounter;
        cloth.itemAmount = 1;
        items.Add(cloth);

        // id = 6
        helmet = new Helmet("Barbarian's Helmet", 1);
        helmet.itemID = ++itemIDCounter;
        helmet.itemAmount = 1;
        items.Add(helmet);

        // id = 7
        cloth = new Cloth("Barbarian's Outfit", 1);
        cloth.itemID = ++itemIDCounter;
        cloth.itemAmount = 1;
        items.Add(cloth);

        // id = 8
        helmet = new Helmet("Paladin's Helmet", 1);
        helmet.itemID = ++itemIDCounter;
        helmet.itemAmount = 1;
        items.Add(helmet);

        // id = 9
        cloth = new Cloth("Paladin's Outfit", 1);
        cloth.itemID = ++itemIDCounter;
        cloth.itemAmount = 1;
        items.Add(cloth);

        // id = 10
        helmet = new Helmet("Alchemist's Mask", 1);
        helmet.itemID = ++itemIDCounter;
        helmet.itemAmount = 1;
        items.Add(helmet);

        // id = 11
        cloth = new Cloth("Alchemist's Outfit", 1);
        cloth.itemID = ++itemIDCounter;
        cloth.itemAmount = 1;
        items.Add(cloth);

        // id = 12
        helmet = new Helmet("Bard's Hat", 1);
        helmet.itemID = ++itemIDCounter;
        helmet.itemAmount = 1;
        items.Add(helmet);

        // id = 13
        cloth = new Cloth("Bard's Outfit", 1);
        cloth.itemID = ++itemIDCounter;
        cloth.itemAmount = 1;
        items.Add(cloth);

        // id = 14
        helmet = new Helmet("Magician's Hat", 1);
        helmet.itemID = ++itemIDCounter;
        helmet.itemAmount = 1;
        items.Add(helmet);

        // id = 15
        cloth = new Cloth("Magician's Robes", 1);
        cloth.itemID = ++itemIDCounter;
        cloth.itemAmount = 1;
        items.Add(cloth);

        // id = 16
        helmet = new Helmet("Spellsword's Helmet", 1);
        helmet.itemID = ++itemIDCounter;
        helmet.itemAmount = 1;
        items.Add(helmet);

        // id = 17
        cloth = new Cloth("Spellsword's Outfit", 1);
        cloth.itemID = ++itemIDCounter;
        cloth.itemAmount = 1;
        items.Add(cloth);

        // id = 18
        meleeWeapon = new MeleeWeapon("Basic Sword", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 19
        meleeWeapon = new MeleeWeapon("Basic Khopesh", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 20
        meleeWeapon = new MeleeWeapon("Basic Scimitar", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 21
        meleeWeapon = new MeleeWeapon("Basic Sword Fragment", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 22
        meleeWeapon = new MeleeWeapon("Basic Dagger", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 23
        meleeWeapon = new MeleeWeapon("Basic Flamberge", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 24
        meleeWeapon = new MeleeWeapon("Basic Axe", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 25
        meleeWeapon = new MeleeWeapon("Basic Trident", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 26
        meleeWeapon = new MeleeWeapon("Basic Lute", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 27
        meleeWeapon = new MeleeWeapon("Basic Mace", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 28
        meleeWeapon = new MeleeWeapon("Basic Wand", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 29
        meleeWeapon = new MeleeWeapon("Basic Spear", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 30
        meleeWeapon = new MeleeWeapon("Basic Stake", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 31
        meleeWeapon = new MeleeWeapon("Basic Needle", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 32
        rangeWeapon = new RangeWeapon("Basic Crossbow", 10, "Arrow Basic");
        rangeWeapon.itemID = ++itemIDCounter;
        rangeWeapon.itemAmount = 1;
        items.Add(rangeWeapon);

        // id = 33
        meleeWeapon = new MeleeWeapon("Fire Sword", 100, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 34
        meleeWeapon = new MeleeWeapon("Fire Khopesh", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 35
        meleeWeapon = new MeleeWeapon("Fire Scimitar", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 36
        meleeWeapon = new MeleeWeapon("Fire Sword Fragment", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 37
        meleeWeapon = new MeleeWeapon("Fire Dagger", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 38
        meleeWeapon = new MeleeWeapon("Fire Flamberge", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 39
        meleeWeapon = new MeleeWeapon("Fire Axe", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 40
        meleeWeapon = new MeleeWeapon("Fire Trident", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 41
        meleeWeapon = new MeleeWeapon("Fire Lute", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 42
        meleeWeapon = new MeleeWeapon("Fire Mace", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 43
        rangeWeapon = new RangeWeapon("Fire Wand", 10, "Fire Bolt");
        rangeWeapon.itemID = ++itemIDCounter;
        rangeWeapon.itemAmount = 1;
        items.Add(rangeWeapon);

        // id = 44
        meleeWeapon = new MeleeWeapon("Fire Spear", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 45
        meleeWeapon = new MeleeWeapon("Fire Stake", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 46
        meleeWeapon = new MeleeWeapon("Fire Needle", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 47
        rangeWeapon = new RangeWeapon("Fire Crossbow", 10, "Arrow Fire");
        rangeWeapon.itemID = ++itemIDCounter;
        rangeWeapon.itemAmount = 1;
        items.Add(rangeWeapon);

        // id = 48
        meleeWeapon = new MeleeWeapon("Blood Sword", 100, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 49
        meleeWeapon = new MeleeWeapon("Blood Khopesh", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 50
        meleeWeapon = new MeleeWeapon("Blood Scimitar", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 51
        meleeWeapon = new MeleeWeapon("Blood Sword Fragment", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 52
        meleeWeapon = new MeleeWeapon("Blood Dagger", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 53
        meleeWeapon = new MeleeWeapon("Blood Flamberge", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 54
        meleeWeapon = new MeleeWeapon("Blood Axe", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 55
        meleeWeapon = new MeleeWeapon("Blood Trident", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 56
        meleeWeapon = new MeleeWeapon("Blood Lute", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 57
        meleeWeapon = new MeleeWeapon("Blood Mace", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 58
        rangeWeapon = new RangeWeapon("Blood Wand", 10, "Bolt Blood");
        rangeWeapon.itemID = ++itemIDCounter;
        rangeWeapon.itemAmount = 1;
        items.Add(rangeWeapon);

        // id = 59
        meleeWeapon = new MeleeWeapon("Blood Spear", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 60
        meleeWeapon = new MeleeWeapon("Blood Stake", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 61
        meleeWeapon = new MeleeWeapon("Blood Needle", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 62
        rangeWeapon = new RangeWeapon("Blood Crossbow", 10, "Arrow Blood");
        rangeWeapon.itemID = ++itemIDCounter;
        rangeWeapon.itemAmount = 1;
        items.Add(rangeWeapon);

        // id = 63
        meleeWeapon = new MeleeWeapon("Holy Sword", 100, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 64
        meleeWeapon = new MeleeWeapon("Holy Khopesh", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 65
        meleeWeapon = new MeleeWeapon("Holy Scimitar", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 66
        meleeWeapon = new MeleeWeapon("Holy Sword Fragment", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 67
        meleeWeapon = new MeleeWeapon("Holy Dagger", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 68
        meleeWeapon = new MeleeWeapon("Holy Flamberge", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 69
        meleeWeapon = new MeleeWeapon("Holy Axe", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 70
        meleeWeapon = new MeleeWeapon("Holy Trident", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 71
        meleeWeapon = new MeleeWeapon("Holy Lute", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 72
        meleeWeapon = new MeleeWeapon("Holy Mace", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 73
        meleeWeapon = new MeleeWeapon("Holy Wand", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 74
        meleeWeapon = new MeleeWeapon("Holy Spear", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 75
        meleeWeapon = new MeleeWeapon("Holy Stake", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 76
        meleeWeapon = new MeleeWeapon("Holy Needle", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 77
        rangeWeapon = new RangeWeapon("Holy Crossbow", 10, "Arrow Holy");
        rangeWeapon.itemID = ++itemIDCounter;
        rangeWeapon.itemAmount = 1;
        items.Add(rangeWeapon);

        // id = 78
        meleeWeapon = new MeleeWeapon("Occult Sword", 100, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 79
        meleeWeapon = new MeleeWeapon("Occult Khopesh", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 80
        meleeWeapon = new MeleeWeapon("Occult Scimitar", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 81
        meleeWeapon = new MeleeWeapon("Occult Sword Fragment", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 82
        meleeWeapon = new MeleeWeapon("Occult Dagger", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 83
        meleeWeapon = new MeleeWeapon("Occult Flamberge", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 84
        meleeWeapon = new MeleeWeapon("Occult Axe", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 85
        meleeWeapon = new MeleeWeapon("Occult Trident", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 86
        meleeWeapon = new MeleeWeapon("Occult Lute", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 87
        meleeWeapon = new MeleeWeapon("Occult Mace", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 88
        meleeWeapon = new MeleeWeapon("Occult Wand", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 89
        meleeWeapon = new MeleeWeapon("Occult Spear", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 90
        meleeWeapon = new MeleeWeapon("Occult Stake", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 91
        meleeWeapon = new MeleeWeapon("Occult Needle", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 92
        rangeWeapon = new RangeWeapon("Occult Crossbow", 10, "Arrow Occult");
        rangeWeapon.itemID = ++itemIDCounter;
        rangeWeapon.itemAmount = 1;
        items.Add(rangeWeapon);

        // id = 93
        meleeWeapon = new MeleeWeapon("Fungus Sword", 100, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 94
        meleeWeapon = new MeleeWeapon("Fungus Khopesh", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 95
        meleeWeapon = new MeleeWeapon("Fungus Scimitar", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 96
        meleeWeapon = new MeleeWeapon("Fungus Sword Fragment", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 97
        meleeWeapon = new MeleeWeapon("Fungus Dagger", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 98
        meleeWeapon = new MeleeWeapon("Fungus Flamberge", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 99
        meleeWeapon = new MeleeWeapon("Fungus Axe", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 100
        meleeWeapon = new MeleeWeapon("Fungus Trident", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 101
        meleeWeapon = new MeleeWeapon("Fungus Lute", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 102
        meleeWeapon = new MeleeWeapon("Fungus Mace", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 103
        meleeWeapon = new MeleeWeapon("Fungus Wand", 10, true);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 104
        meleeWeapon = new MeleeWeapon("Fungus Spear", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 105
        meleeWeapon = new MeleeWeapon("Fungus Stake", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 106
        meleeWeapon = new MeleeWeapon("Fungus Needle", 10, false);
        meleeWeapon.itemID = ++itemIDCounter;
        meleeWeapon.itemAmount = 1;
        items.Add(meleeWeapon);

        // id = 107
        rangeWeapon = new RangeWeapon("Fungus Crossbow", 10, "Arrow Fungus");
        rangeWeapon.itemID = ++itemIDCounter;
        rangeWeapon.itemAmount = 1;
        items.Add(rangeWeapon);

        // id = 108
        helmet = new Helmet("Exorcist's Hat", 5);
        helmet.itemID = ++itemIDCounter;
        helmet.itemAmount = 1;
        items.Add(helmet);

        // id = 109
        cloth = new Cloth("Exorcist's Outfit", 5);
        cloth.itemID = ++itemIDCounter;
        cloth.itemAmount = 1;
        items.Add(cloth);


        ElementStone stone;
        // id = 110
        stone = new ElementStone("Basic Stone", ElementStone.ElementType.Basic);
        stone.itemID = ++itemIDCounter;
        stone.itemAmount = 1;
        items.Add(stone);

        // id = 111
        stone = new ElementStone("Fire Stone", ElementStone.ElementType.Fire);
        stone.itemID = ++itemIDCounter;
        stone.itemAmount = 1;
        items.Add(stone);

        // id = 112
        stone = new ElementStone("Blood Stone", ElementStone.ElementType.Blood);
        stone.itemID = ++itemIDCounter;
        stone.itemAmount = 1;
        items.Add(stone);

        // id = 113
        stone = new ElementStone("Fungus Stone", ElementStone.ElementType.Fungus);
        stone.itemID = ++itemIDCounter;
        stone.itemAmount = 1;
        items.Add(stone);

        // id = 114
        stone = new ElementStone("Holy Stone", ElementStone.ElementType.Holy);
        stone.itemID = ++itemIDCounter;
        stone.itemAmount = 1;
        items.Add(stone);

        // id = 115
        stone = new ElementStone("Occult Stone", ElementStone.ElementType.Occult);
        stone.itemID = ++itemIDCounter;
        stone.itemAmount = 1;
        items.Add(stone);

        // id = 116
        HpPotion hp = new HpPotion("Healing Potion", 45);
        hp.itemID = ++itemIDCounter;
        hp.itemAmount = 1;
        items.Add(hp);

        // id = 117
        SpPotion mp = new SpPotion("Magic Potion", 45);
        mp.itemID = ++itemIDCounter;
        mp.itemAmount = 1;
        items.Add(mp);


        /*
        MeleeWeapon axe = new MeleeWeapon("Axe", 100);
        //axe.itemName = "Axe";
        axe.itemID = ++itemIDCounter;
        axe.itemDes = "mighty axe";
        axe.itemAmount = 1;
        //axe.itemEffects.Add(new OnEquipAddDmg(2));
        //axe.InitEffect();
        //axe.LoadIcon();
        //axe.LoadWeaponSprite();
        items.Add(axe);

        RangeWeapon gun = new RangeWeapon("Gun", 999, Constants.ProjectilePrefabPath + "CurvedArrow");
        //alchemistPotion.itemName = "Alchemist Potion";
        gun.itemID = ++itemIDCounter;
        gun.itemDes = "Gun";
        gun.itemAmount = 1;
        //alchemistPotion.equipType = EquipableItem.EquipType.Weapon;
        //alchemistPotion.itemEffects.Add(new OnEquipAddDmg(2));
        //alchemistPotion.InitEffect();
        //alchemistPotion.LoadIcon();
        //alchemistPotion.LoadWeaponSprite();
        items.Add(gun);
        

        RangeWeapon alchemistPotion = new RangeWeapon("Alchemist Potion", 10, "");
        //alchemistPotion.itemName = "Alchemist Potion";
        alchemistPotion.itemID = ++itemIDCounter;
        alchemistPotion.itemDes = "Alchemist Potion";
        alchemistPotion.itemAmount = 1;
        //alchemistPotion.equipType = EquipableItem.EquipType.Weapon;
        //alchemistPotion.itemEffects.Add(new OnEquipAddDmg(2));
        //alchemistPotion.InitEffect();
        //alchemistPotion.LoadIcon();
        //alchemistPotion.LoadWeaponSprite();
        items.Add(alchemistPotion);

        /* TODO: Important Structure Changed.
         * 		 need to redesign item creation
         * 
        */

        /*
        EquipableItem axe = new EquipableItem ();
        axe.itemName = "Axe";
        axe.itemID = ++itemIDCounter;
        axe.itemDes = "mighty axe";
        axe.itemAmount = 1;
        axe.equipType = EquipableItem.EquipType.Weapon;
        axe.itemEffects.Add (new OnEquipAddDmg (2));
        axe.InitEffect ();
        axe.LoadIcon ();
        axe.LoadWeaponSprite ();
        items.Add (axe);
        */


        /*
        EquipableItem Khopesh = new EquipableItem();
        Khopesh.itemName = "Khopesh";
        Khopesh.itemID = ++itemIDCounter;
        Khopesh.itemDes = "Khopesh";
        Khopesh.itemAmount = 1;
        Khopesh.equipType = EquipableItem.EquipType.Weapon;
        Khopesh.itemEffects.Add(new OnEquipAddDmg(2));
        Khopesh.InitEffect();
        Khopesh.LoadIcon();
        Khopesh.LoadWeaponSprite();
        items.Add(Khopesh);

        EquipableItem Rapier = new EquipableItem();
        Rapier.itemName = "Rapier";
        Rapier.itemID = ++itemIDCounter;
        Rapier.itemDes = "Rapier";
        Rapier.itemAmount = 1;
        Rapier.equipType = EquipableItem.EquipType.Weapon;
        Rapier.itemEffects.Add(new OnEquipAddDmg(2));
        Rapier.InitEffect();
        Rapier.LoadIcon();
        Rapier.LoadWeaponSprite();
        items.Add(Rapier);

        EquipableItem Sword = new EquipableItem();
        Sword.itemName = "Sword";
        Sword.itemID = ++itemIDCounter;
        Sword.itemDes = "Sword";
        Sword.itemAmount = 1;
        Sword.equipType = EquipableItem.EquipType.Weapon;
        Sword.itemEffects.Add(new OnEquipAddDmg(2));
        Sword.InitEffect();
        Sword.LoadIcon();
        Sword.LoadWeaponSprite();
        items.Add(Sword);

        EquipableItem WizardStaff = new EquipableItem();
        WizardStaff.itemName = "Wizard Staff";
        WizardStaff.itemID = ++itemIDCounter;
        WizardStaff.itemDes = "Wizard Staff";
        WizardStaff.itemAmount = 1;
        WizardStaff.equipType = EquipableItem.EquipType.Weapon;
        WizardStaff.itemEffects.Add(new OnEquipAddDmg(2));
        WizardStaff.InitEffect();
        WizardStaff.LoadIcon();
        WizardStaff.LoadWeaponSprite();
        items.Add(WizardStaff);

        EquipableItem Gun = new EquipableItem();
        Gun.itemName = "Gun";
        Gun.itemID = ++itemIDCounter;
        Gun.itemDes = "Gun";
        Gun.itemAmount = 1;
        Gun.equipType = EquipableItem.EquipType.Weapon;
        Gun.itemEffects.Add(new OnEquipAddDmg(2));
        Gun.InitEffect();
        Gun.LoadIcon();
        Gun.LoadWeaponSprite();
        items.Add(Gun);

        ConsumableItem rawTurkey = new ConsumableItem();
        rawTurkey.itemName = "Raw Turkey";
        rawTurkey.itemID = ++itemIDCounter;
        rawTurkey.itemDes = "Surprisingly nourishing, this item heals the player character for 2 points.";
        rawTurkey.itemAmount = 1;
        rawTurkey.itemEffects.Add(new OnUseHeal(2));
        rawTurkey.InitEffect();
        rawTurkey.LoadIcon();
        items.Add(rawTurkey);

        ConsumableItem refreshingWater = new ConsumableItem();
        refreshingWater.itemName = "Refreshing Water";
        refreshingWater.itemID = ++itemIDCounter;
        refreshingWater.itemDes = "Soothingly hydrating, this item immediately gives the player 4 SP.";
        refreshingWater.itemAmount = 1;
        refreshingWater.itemEffects.Add(new OnUseRecoverSP(4));
        refreshingWater.InitEffect();
        refreshingWater.LoadIcon();
        items.Add(refreshingWater);
        */

    }

    public Item GetItemByID(int id)
    {
        // search through database
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].itemID == id)
            {
                Item item = items[i];
                return items[i];
            }
        }

        Debug.Log("item not found");
        return null;
    }

    public Item GetItemByName(string name)
    {
        // search through database
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].itemName == name)
                return items[i];
        }

        Debug.Log(name + "  not found");
        return null;
    }


    public int GetItemAmtTotal()
    {
        return items.Count;
    }

    public Item GetRandomItem()
    {
        int rdIndex = Random.Range(0, items.Count);

        Item item = items[rdIndex];

        return item;
    }
}
