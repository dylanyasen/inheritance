﻿using UnityEngine;
using System.Collections;

public class Goldbag : Pickup
{
    public int amt { get; private set; }

    public void SetAmt(int amt)
    {
        this.amt = amt;
    }

    public override void PickUp()
    {
        Player p = (Player)triggeredEntity;
        p.AddGold(amt);

        GM.instance.FloatText("+ " + amt + " gold", Camera.main.WorldToScreenPoint(p.m_trans.position), Color.yellow);
    }
}
