﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Item
{
    public string itemName;
    public int itemID;
    public string itemDes;
    public Sprite itemIcon;
    public Sprite itemSprite;
    public int itemAmount;

    //public Entity m_entity; // reference here or in equip function

    public Item(string name)
    {
        itemName = name;
        itemAmount = 1;
        LoadIcon();
    }

    public Item()
    {
        // empty
        itemID = -1;
    }

    public void LoadIcon()
    {
        if (ItemDatabase.instance.iconSpriteSheet.ContainsKey(itemName))
            itemIcon = ItemDatabase.instance.iconSpriteSheet[itemName];
        else
            Debug.Log(itemName + "icon not exist in spritesheet");
    }


    public void LoadEquipmentSprite()
    {
        if (ItemDatabase.instance.weaponSpriteSheet.ContainsKey(itemName))
            itemSprite = ItemDatabase.instance.weaponSpriteSheet[itemName];
        else
            Debug.Log(itemName + " weaponsprite not exist in spritesheet");
    }

}
