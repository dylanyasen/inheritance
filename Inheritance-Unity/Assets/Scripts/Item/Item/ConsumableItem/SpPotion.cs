﻿using UnityEngine;
using System.Collections;

public class SpPotion : ConsumableItem
{

    private int spRegen;

    public SpPotion(string name, int spRegen, string particlePath = "")
        : base(name)
    {
        this.spRegen = spRegen;
    }

    public override bool Use(Player player)
    {
        //base.Use(player);
        player.m_stats.AddSP(spRegen);

        return true;
    }
}
