﻿using UnityEngine;
using System.Collections;

public class ElementStone : ConsumableItem
{

    public enum ElementType
    {
        Basic,
        Fire,
        Blood,
        Holy,
        Occult,
        Fungus
    }

    public ElementType element;

    public ElementStone(string name, ElementType e)
        : base(name)
    {
        element = e;
    }

    public override bool Use(Player player)
    {
        //base.Use(player);

        Weapon weapon = player.weaponManager.currentWeapon;

        if (weapon == null)
            return false;

        string name = weapon.itemName;
        string[] subStr = name.Split(new char[] { ' ' });

        // get item name with out type
        name = subStr[subStr.Length - 1];

        // add new type
        name = element.ToString() + " " + name;

        Debug.Log("item name " + name);

        Item newWeapon = ItemDatabase.instance.GetItemByName(name);

        Debug.Log(newWeapon.itemID + " " + newWeapon.itemName);

        // weapon slot
        Item currentWeapon = player.charPanel.items[3];

        // unequip current weapon
        player.weaponManager.UnEquip(weapon);

        // remove from current slot
        player.charPanel.charSlots[3].UnequipInSlotItem();

        // 
        player.AddItemToInventory(newWeapon);

        return true;
    }
}
