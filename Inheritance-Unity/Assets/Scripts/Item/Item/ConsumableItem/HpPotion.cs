﻿using UnityEngine;
using System.Collections;

public class HpPotion : ConsumableItem
{
    private int hpRegen;

    public HpPotion(string name, int hpRegen, string particlePath = "")
        : base(name)
    {
        this.hpRegen = hpRegen;
    }

    public override bool Use(Player player)
    {
        //base.Use(player);
        player.m_stats.AddHP(hpRegen);

        return true;
    }
}
