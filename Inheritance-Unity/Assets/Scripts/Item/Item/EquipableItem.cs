﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class EquipableItem : Item
{
    public enum EquipType
    {
        // order is the same as charslot order
        Head,
        Body,
        Legs,
        Weapon,
        Arms,
        Accessory
    }

    public EquipType equipType { get; set; }
    //public List<OnEquipItemEffect> itemEffects = new List<OnEquipItemEffect>();

    public int indexInEquipmentSlot { get; protected set; }

    /*
    delegate void EquipEffectDelegate();
    EquipEffectDelegate itemEffectDelegate;
    */
     
    public EquipableItem(string name)
        : base(name)
    {
        //LoadEquipmentSprite();
    }

    //public void InitEffect()
    //{
    //    for (int i = 0; i < itemEffects.Count; i++)
    //    {
    //        itemEffectDelegate += itemEffects[i].Equip;
    //    }
    //}

    //public void ActiveItemEffect()
    //{
    //    itemEffectDelegate();
    //}

    //public void DeactivateItemEffect()
    //{

    //    for (int i = 0; i < itemEffects.Count; i++)
    //    {
    //        itemEffectDelegate -= itemEffects[i].Equip;
    //        itemEffectDelegate += itemEffects[i].UnEquip;
    //    }

    //    itemEffectDelegate();
    //}

    public virtual void OnEquip(Entity entity)
    {

        Debug.Log("EquipableItem base OnEquip");
    }

    public virtual void UnEquip(Entity entity)
    {
        Debug.Log("EquipableItem base UnEquip");
    }

}