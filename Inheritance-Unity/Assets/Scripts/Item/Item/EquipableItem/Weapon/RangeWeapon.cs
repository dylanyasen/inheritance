﻿using UnityEngine;
using System.Collections;

public class RangeWeapon : Weapon
{
    public GameObject _projectile { get; private set; }

    // TODO:
    public string projectilePrefabName; // to load at run time && can write to file as well

    public RangeWeapon(string name, int dmg, string projectileName, string particlePath = "")
        : base(name, dmg, particlePath)
    {
        _projectile = Resources.Load<GameObject>(Constants.ProjectilePrefabPath + projectileName);
    }

    public override void OnEquip(Entity entity)
    {
        base.OnEquip(entity);
    }

    public override void Attack(Vector2 dir, Entity entity)
    {
        if (_projectile == null)
            return;

        GameObject proj = MonoBehaviour.Instantiate(_projectile, entity.GetPos(), Quaternion.identity) as GameObject;

        ProjectileBase baseProj = proj.GetComponent<ProjectileBase>();
        baseProj.SetDmg(entity.m_stats.cur_dmg);
        baseProj.Fire(dir);
    }
}
