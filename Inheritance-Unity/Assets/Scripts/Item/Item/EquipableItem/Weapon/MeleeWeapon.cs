﻿using UnityEngine;
using System.Collections;

public class MeleeWeapon : Weapon
{
    public MeleeWeapon(string name, int dmg, bool swing, string particlePath = "")
        : base(name, dmg, particlePath)
    {
        this.isSwing = swing;
    }

    public override void OnEquip(Entity entity)
    {
        base.OnEquip(entity);
    }

    public override void Attack(Vector2 dir, Entity entity)
    {

    }
}
