﻿using UnityEngine;
using System.Collections;

public abstract class Weapon : EquipableItem
{
    /*
     * TODO:
     * may be a enum for different element attack
    */

    public bool isSwing { get; protected set; }

    public int _dmg { get; private set; }
    public float _coolDown { get; private set; }

    public float _criticalProb { get; private set; }

    public GameObject _particlePrefab { get; private set; }

    public Weapon(string name, int dmg, string particlePath)
        : base(name)
    {
        _dmg = dmg;

        indexInEquipmentSlot = 3;

        if (particlePath != "")
            _particlePrefab = Resources.Load<GameObject>(particlePath);

        LoadWeaponSprite();
    }

    protected void LoadWeaponSprite()
    {
        if (ItemDatabase.instance.weaponSpriteSheet.ContainsKey(itemName))
            itemSprite = ItemDatabase.instance.weaponSpriteSheet[itemName];
        else
            Debug.Log(itemName + " weaponsprite not exist in spritesheet");
    }

    public override void OnEquip(Entity entity)
    {
        base.OnEquip(entity);

        entity.m_stats.AddExtraDMG(_dmg);
    }

    public override void UnEquip(Entity entity)
    {
        base.UnEquip(entity);

        entity.m_stats.AddExtraDMG(-_dmg);
    }

    public abstract void Attack(Vector2 dir, Entity entity);

}
