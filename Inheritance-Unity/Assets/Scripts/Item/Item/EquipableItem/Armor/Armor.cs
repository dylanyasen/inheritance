﻿using UnityEngine;
using System.Collections;

public abstract class Armor : EquipableItem
{
    public int _def;
    public Sprite[] sprites { get; protected set; }

    public Armor(string name, int def)
        : base(name)
    {
        _def = def;

        //Debug.Log(name);

        sprites = new Sprite[3];
    }
}
