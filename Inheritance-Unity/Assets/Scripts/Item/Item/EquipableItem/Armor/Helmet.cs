﻿using UnityEngine;
using System.Collections;

public class Helmet : Armor
{
    public Helmet(string name, int def)
        : base(name, def)
    {
        indexInEquipmentSlot = 0;

        //Debug.Log(ItemDatabase.instance);
        //Debug.Log(ItemDatabase.instance.armorSpriteSheet);

        string helmetSide = itemName + "_helmet_side";
        if (ItemDatabase.instance.armorSpriteSheet.ContainsKey(helmetSide))
            sprites[0] = ItemDatabase.instance.armorSpriteSheet[helmetSide];

        string helmetFront = itemName + "_helmet_front";
        if (ItemDatabase.instance.armorSpriteSheet.ContainsKey(helmetFront))
            sprites[1] = ItemDatabase.instance.armorSpriteSheet[helmetFront];

        string helmetBack = itemName + "_helmet_back";
        if (ItemDatabase.instance.armorSpriteSheet.ContainsKey(helmetBack))
            sprites[2] = ItemDatabase.instance.armorSpriteSheet[helmetBack];

        //Debug.Log("helmet passed");
    }

}
