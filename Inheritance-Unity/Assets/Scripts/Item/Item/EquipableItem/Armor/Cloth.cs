﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cloth : Armor
{
    public Sprite sleeveSprite { get; private set; }
    public Sprite shoulderSprite { get; private set; }

    // 0 --> side
    // 1 --> front
    // 2 --> back

    public Cloth(string name, int def)
        : base(name, def)
    {
        // index in char slot
        indexInEquipmentSlot = 1;

        string clothSide = itemName + "_clothes_side";
        if (ItemDatabase.instance.armorSpriteSheet.ContainsKey(clothSide))
            sprites[0] = ItemDatabase.instance.armorSpriteSheet[clothSide];

        string clothFront = itemName + "_clothes_front";
        if (ItemDatabase.instance.armorSpriteSheet.ContainsKey(clothFront))
            sprites[1] = ItemDatabase.instance.armorSpriteSheet[clothFront];


        string clothBack = itemName + "_clothes_back";
        if (ItemDatabase.instance.armorSpriteSheet.ContainsKey(clothBack))
            sprites[2] = ItemDatabase.instance.armorSpriteSheet[clothBack];

        //Debug.Log("cloth passed");

        string sleeve = itemName + "_clothes_sleeve";
        if (ItemDatabase.instance.armorSpriteSheet.ContainsKey(sleeve))
            sleeveSprite = ItemDatabase.instance.armorSpriteSheet[sleeve];
        //Debug.Log("sleeve passed");

        string shoulder = itemName + "_clothes_shoulder";
        if (ItemDatabase.instance.armorSpriteSheet.ContainsKey(shoulder))
            shoulderSprite = ItemDatabase.instance.armorSpriteSheet[shoulder];
        //Debug.Log(" shoulder passed");

    }
}
