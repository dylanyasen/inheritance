﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Loot
{
    public int itemID;
    public float probability;

    public bool Drop()
    {
        float prob = Random.value * 100;

        return prob <= probability ? true : false;
    }
}
