﻿using UnityEngine;
using System.Collections;

public class AddMaterialTochild : MonoBehaviour
{

    public Material material;

    // Use this for initialization
    void Start()
    {

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetComponent<SpriteRenderer>().material = material;
        }

    }

}
