﻿using UnityEngine;
using System.Collections;

public class AITesting : MonoBehaviour {

	public enum State{
		Idle,
		Patrol,
		Alert,
		Attack,
		Count
	}

	delegate void AIDelegate();
	AIDelegate[] aiDelegate;

	public State aiState;

	private bool inState = false;

	void Awake(){

		aiDelegate = new AIDelegate[(int)State.Count];

		aiDelegate [(int)State.Alert] = AlertState;
		aiDelegate [(int)State.Attack] = AttackState;
		aiDelegate [(int)State.Idle] = IdleState;
		aiDelegate [(int)State.Patrol] = PatrolState;
	}

	void Start(){
	
		//=Debug.Log (Time.time);
		StartCoroutine (PauseForRandomSec());
		Debug.Log (Time.time);
	}

	void IdleState(){
	
		if (!inState) {
			inState = true;
			print("enter idle" + Time.time);
			StartCoroutine (PauseForRandomSec());
			print ("end pause " + Time.time);

			inState = false;
			aiState = State.Patrol;
		}

	}

	void PatrolState(){
	
	}

	void AlertState(){
	
	}

	void AttackState(){

	}

	IEnumerator PauseForRandomSec(){
		int pause = Random.Range (2,3);

		print ("pause time " + pause);

		Debug.Log (Time.time);
		yield return new WaitForSeconds (2f);
		yield return null;
	}

	void Update () {
	
		//if(aiDelegate != null)
			//aiDelegate[(int)aiState]();
	}
}
