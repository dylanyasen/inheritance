﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BlackScreenMask : MonoBehaviour
{
    public static BlackScreenMask instance;

    public float Duration = 2;

    private Text text;
    private Image bg;

    private Animator textAnim;

    private string[] labelText = { "Descending", "Ascending" };

    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(transform.parent.gameObject);
        DontDestroyOnLoad(transform.GetChild(0).gameObject);

        text = transform.GetChild(0).GetComponent<Text>();
        textAnim = transform.GetChild(0).GetComponent<Animator>();
        bg = GetComponent<Image>();

        gameObject.SetActive(false);
        //Hide();
    }

    public void SetText(string st)
    {
        text.text = st;
    }

    // 0 down 1 up 
    public void Show(int dir)
    {
        text.text = labelText[dir];
        gameObject.SetActive(true);

        GM.instance._camController.gameObject.SetActive(false);

        GM.instance._player.statsGUI.gameObject.SetActive(false);


        StartCoroutine(Wait(Duration));
    }

    void Hide()
    {
        gameObject.SetActive(false);

        if (GM.instance._player)
            GM.instance._player.m_controller.SetCanControl(true);

        GM.instance._camController.gameObject.SetActive(true);

        GM.instance._player.statsGUI.gameObject.SetActive(true);

        GM.instance._levelManager.InitLevel();
    }

    IEnumerator Wait(float sec)
    {


        yield return new WaitForSeconds(sec);



        Hide();
    }

}
