﻿using UnityEngine;
using System.Collections;

public class ParticleRenderLayer : MonoBehaviour
{

    void Start()
    {
        // Set the sorting layer of the particle system.
        GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = "fx";
        //particleSystem.renderer.sortingOrder = 2;
    }
}
