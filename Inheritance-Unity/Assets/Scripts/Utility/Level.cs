﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Level : MonoBehaviour
{
    //private List<Transform> enemySpwanPoints = new List<Transform>();

    // TODO: move this to resouce manager
    public string[] enemyPrefabName;

    public int enemyAmt = 5;

    private Transform[] spawnPoints;

    public List<Door> doors { get; private set; }

    public List<Enemy> enemyList { get; private set; }

    public Tile[,] Tiles { get; private set; }

    void Awake()
    {
        Transform doorParent = transform.GetChild(1);
        int doorAmt = doorParent.childCount;
        doors = new List<Door>();
        for (int i = 0; i < doorAmt; i++)
            doors.Add(doorParent.GetChild(i).GetComponent<Door>());


        //transform.GetChild(2) != null
        if (transform.childCount > 2)
        {
            Transform child = transform.GetChild(2);

            if (child.name == "spawnPoint")
            {
                spawnPoints = new Transform[child.childCount];

                for (int i = 0; i < child.childCount; i++)
                    spawnPoints[i] = child.GetChild(i);
            }
        }

        // set up tile map
        SetupTileDataStruct();

        // init enemy list
        enemyList = new List<Enemy>();
        // load enemy prefab resources
        // TODO: load should be in resource manager

        if (spawnPoints != null && enemyPrefabName.Length >= 1)
            LoadCreateEnemyPrefab();

        // hide level
        Show(false);
    }

    void Start()
    {
    }

    private void LoadCreateEnemyPrefab()
    {
        // load prefabs ==> resource manager
        List<GameObject> prefabs = new List<GameObject>();
        foreach (string name in enemyPrefabName)
            prefabs.Add(Resources.Load<GameObject>(Constants.EnemyPrefabPath + name));


        for (int i = 0; i < enemyAmt; i++)
        {
            int randIndex = Random.Range(0, spawnPoints.Length);
            Vector2 pos = spawnPoints[randIndex].position;

            int randomEnemy = Random.Range(0, prefabs.Count);

            // create enemy
            GameObject obj = Instantiate(prefabs[randomEnemy], pos, Quaternion.identity) as GameObject;


            // set parent
            obj.transform.parent = transform;

            // get enemy component
            Enemy enemy = obj.GetComponent<Enemy>();

            // store enemy
            enemyList.Add(enemy);

            // hide enemy first
            enemy.Show(false);
        }
    }

    public void SpawnEnemy()
    {
        if (enemyList == null || enemyList.Count < 1)
            return;

        foreach (Enemy enemy in enemyList)
        {
            enemy.m_stats.ResetStats();

            enemy.Show(true);
        }
    }

    public void HideEnemy()
    {
        foreach (Enemy enemy in enemyList)
        {
            enemy.m_stats.ResetStats();

            enemy.Show(false);
        }
    }

    /*
    public Vector2 GetEntryPointPos(int index)
    {
        if (index < 0 || index >= entryPoints.Length)
        {
            Debug.Log("get entry point pos out of bounds. return first pos instead");
            index = 0;
        }

        return entryPoints[index].position;
    }
     */

    public Door GetDoor(Door.DoorType doorType)
    {
        for (int i = 0; i < doors.Count; i++)
        {
            if (doors[i].type == doorType)
                return doors[i];
        }

        return null;
    }

    public Vector2 GetDoorOffsetPos(Door.DoorType doorType)
    {
        for (int i = 0; i < doors.Count; i++)
        {
            if (doors[i].type == doorType)
                return ((Vector2)doors[i].m_trans.position - Vector2.up * 150);
        }

        Debug.Log("door not found");
        return Vector2.zero;
    }

    public void Show(bool Show)
    {
        gameObject.SetActive(Show);
    }

    public Vector2 GetRandomPosInCircle(float radius = 1)
    {
        return Random.insideUnitCircle * radius;
    }


    void SetupTileDataStruct()
    {
        // care child index
        Transform tileParentObj = transform.GetChild(0);

        // store tiles
        List<Transform> tiles = new List<Transform>();
        for (int i = 0; i < tileParentObj.childCount; i++)
        {
            if (tileParentObj.GetChild(i).tag == "Tile")
                tiles.Add(tileParentObj.GetChild(i));
        }

        // store postion to find most left / most bottom
        // in order to set 2D array size
        List<float> xPos = new List<float>();
        for (int i = 0; i < tiles.Count; i++)
            xPos.Add(tiles[i].position.x);
        xPos.Sort();
        float xMin = xPos[0];
        float xMax = xPos[xPos.Count - 1];

        //Debug.Log(xMin);
        //Debug.Log(xMax);

        List<float> yPos = new List<float>();
        for (int i = 0; i < tiles.Count; i++)
            yPos.Add(tiles[i].position.y);
        yPos.Sort();
        float yMax = yPos[yPos.Count - 1];
        float yMin = yPos[0];

        // size of tile 2d array
        int xSize = (int)((xMax - xMin) / 100);
        int ySize = (int)((yMax - yMin) / 100);
        Tiles = new Tile[xSize + 1, ySize + 1];

        //Debug.Log("Xsize" + xSize);
        //Debug.Log("Ysize" + ySize);

        //Debug.Log("tile count:" + tiles.Count);

        for (int i = 0; i < tiles.Count; i++)
        {
            Vector2 pos = tiles[i].position;
            int xIndex = (int)((pos.x - xMin) / 100);
            int yIndex = (int)((pos.y - yMin) / 100);

            Tiles[xIndex, yIndex] = tiles[i].GetComponent<Tile>();
            //Debug.Log(Tiles[xIndex, yIndex]);
        }
    }

    /*
void OnDrawGizmos()
{
    for (int i = 0; i < Tiles.GetLength(0); i++)
        for (int j = 0; j < Tiles.GetLength(1); j++)
            if (Tiles[i, j])
            {
                Gizmos.color = Tiles[i, j].GetColor();
                Gizmos.DrawWireCube(Tiles[i, j].GetPos(), Tiles[i, j].GetSize());
            }
    
}
     */

    public Tile GetCurrentTile(Vector2 pos)
    {
        for (int i = 0; i < Tiles.GetLength(0); i++)
            for (int j = 0; j < Tiles.GetLength(1); j++)
                if (Tiles[i, j] && Tiles[i, j].bound.Contains(pos))
                    return Tiles[i, j];

        return null;
    }

    public Vector2 GetCurrentTileIndex(Vector2 pos)
    {
        for (int i = 0; i < Tiles.GetLength(0); i++)
            for (int j = 0; j < Tiles.GetLength(1); j++)
            {
                if (Tiles[i, j] && Tiles[i, j].bound.Contains(pos))
                    return new Vector2(i, j);
            }
        return Vector2.zero;
    }

    public Tile GetRandomTile()
    {
        Tile t = Tiles[Random.Range(0, Tiles.GetLength(0)), Random.Range(0, Tiles.GetLength(1))];

        while (t == null)
            t = Tiles[Random.Range(0, Tiles.GetLength(0)), Random.Range(0, Tiles.GetLength(1))];

        return t;
    }

}
