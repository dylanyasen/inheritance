﻿using UnityEngine;
using System.Collections;

public class PlayerHitBox : HitBox
{
    protected override void Start()
    {
        base.Start();

        tag = "PlayerHitbox";
    }
}
