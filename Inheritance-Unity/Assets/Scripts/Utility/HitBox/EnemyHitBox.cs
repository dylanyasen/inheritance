﻿using UnityEngine;
using System.Collections;

public class EnemyHitBox : HitBox
{

    protected override void Start()
    {
        base.Start();

        tag = "EnemyHitbox";
    
    }   
}
