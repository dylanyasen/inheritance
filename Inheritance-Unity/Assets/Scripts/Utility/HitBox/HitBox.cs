﻿using UnityEngine;
using System.Collections;

/// <summary>
/// generic hit box
/// 4-direction
/// 
/// 
///  clockwise
/// 
/// 
/// </summary>
public abstract class HitBox : MonoBehaviour
{
    public Collider2D[] hitbox { get; private set; }

    public Entity m_entity { get; private set; }

    void Awake()
    {
        int childCount = transform.childCount;

        hitbox = new Collider2D[childCount];

        for (int i = 0; i < childCount; i++)
        {
            hitbox[i] = transform.GetChild(i).GetComponent<Collider2D>();
            hitbox[i].enabled = false;
        }
    }

    protected virtual void Start()
    {

    }

    public void EnableHitBox(int dir)
    {
        hitbox[dir].enabled = true;
    }

    public void DisableHitBox(int dir)
    {
        hitbox[dir].enabled = false;
    }

    public void SetHitBoxRadius(float radius)
    {
        //foreach (Collider2D col in hitbox)
        //    (CircleCollider2D)col.

    }

    // set distance from entity 
    public void SetDis()
    {

    }

    public void SetEntity(Entity entity)
    {
        m_entity = entity;
    }

    public void SetTag(string tag)
    {
        for (int i = 0; i < hitbox.Length; i++)
            hitbox[i].tag = tag;

    }
}
