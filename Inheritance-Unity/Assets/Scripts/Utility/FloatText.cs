﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class FloatText : MonoBehaviour
{
    private Image image;  // image pop up
    private Text text;

    private RectTransform canvas;
    private RectTransform m_rectTrans;

    void Awake()
    {
        text = GetComponent<Text>();

        canvas = GameObject.FindWithTag("Canvas").GetComponent<RectTransform>();
        m_rectTrans = gameObject.GetComponent<RectTransform>();
        m_rectTrans.SetParent(canvas);

        gameObject.SetActive(false);
    }

    public void Pop(string txt)
    {
        text.text = txt;
        gameObject.SetActive(true);
    }

    public void Pop(string txt, Color color)
    {
        text.text = txt;
        text.color = color;
        gameObject.SetActive(true);
    }

    public void Pop(string txt, Color color, int fontSize)
    {
        text.text = txt;
        text.color = color;
        text.fontSize = fontSize;

        gameObject.SetActive(true);
    }

    // TODO: pooling
    void Finish()
    {
        Destroy(gameObject);
    }

    public void SetTextColor(Color color)
    {
        text.color = color;
    }

    public void SetSize(int size)
    {
        text.fontSize = size;
    }

    public void SetFont()
    {

    }

    void Dismiss()
    {
        Destroy(gameObject);
    }
}
