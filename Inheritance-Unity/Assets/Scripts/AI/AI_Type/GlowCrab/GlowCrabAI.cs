﻿using UnityEngine;
using System.Collections;

public class GlowCrabAI : MeleeSimpleAI
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        //base.Update();
        // add type-specific behaviors

        if (FSM.currentState is UnderAttackState || FSM.currentState is DashState || FSM.currentState is JumpState)
            return;

        // no target
        if (target == null)
        {
            if (!(FSM.currentState is IdleState) && !(FSM.currentState is PatrolState))
            {
                FSM.ChangeState(new IdleState(this), true);

                //Debug.Log("target null");
            }

            return;
        }

        disToTarget = Vector2.Distance(transform.position, target.position);

        // trigger alert state
        if (disToTarget < AlertRange)
        {
            if (!(FSM.currentState is AlertState) && !(FSM.currentState is AttackState) && !(FSM.currentState is ChaseState))
            {
                //Debug.Log("current state is " + FSM.currentState + ".  Change to alert state");
                FSM.ChangeState(new AlertState(this, target), true);
            }
        }

        // trigger idle state
        else if (disToTarget > AlertRange)
        {
            if (!(FSM.currentState is IdleState) && !(FSM.currentState is PatrolState))
            {

                //Debug.Log("out of alert range." + "current state is " + FSM.currentState + ".  Change to idle state");

                FSM.ChangeState(new IdleState(this), true);
            }
        }
    }

    public override void Dash()
    {
        m_controller.OnDash(0.5f);
    }

    public override void Jump()
    {
        m_controller.Jump();
        //m_controller.m_isSpecialAttacking = true;
    }

    public override void Alert()
    {
        //Debug.Log("this is melee simple alert");
    }

    public override void Attack()
    {
        //Debug.Log("this is melee simple attack");

        //int dashOrHit = Random.Range(0, 10);

        //if (dashOrHit < 4)
        //{
        //    Debug.Log("random->dash!");
        //    FSM.ChangeState(new DashState(this), true);
        //}

        //else
        //{
        //Debug.Log("random->attack!");
        m_controller.MeleeAttack(target);
        //}
    }

    public override void UnderAttack()
    {
        //Debug.Log("this is melee simple under attack");
        m_controller.UnderAttack();
    }

    public override void Patrol()
    {
        //Debug.Log("this is melee simple patrol");
        m_controller.MoveInRandDir();
    }

    public override void Idle()
    {
        //Debug.Log("this is melee simple idle");
        m_controller.Idle();
    }

    public override void Chase(Transform target)
    {
        //Debug.Log("this is melee simple chase");
        m_controller.Chase(target, 2);

        int i = Random.Range(0, 10);

        if (i <= 5)
        {
            Debug.Log("random==>jump");
            FSM.ChangeState(new JumpState(this), true);
        }
    }
}
