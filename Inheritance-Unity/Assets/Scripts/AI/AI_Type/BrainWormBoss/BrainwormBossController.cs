﻿using UnityEngine;
using System.Collections;

public class BrainwormBossController : BossController
{
    public GameObject summonPrefab;

    protected override void Start()
    {
        base.Start();

        summonPrefab = Resources.Load<GameObject>(Constants.HazardPrefabPath + "Tentacle");
    }

    public override void Summon(Vector2 pos)
    {
        GameObject tantecle = GameObject.Instantiate(summonPrefab, pos, Quaternion.identity) as GameObject;

        // trigger tantecle
        tantecle.GetComponent<Tentacle>().Interact();
        tantecle.GetComponent<Tentacle>().SetSelfDestory(true);

    }

}
