﻿using UnityEngine;
using System.Collections;

public class BrainwormBossAI : AIBoss
{
    public GameObject tentacle;
    private BrainwormBossController controller;

    protected override void Start()
    {
        base.Start();

        tentacle = Resources.Load<GameObject>("");

        controller = (BrainwormBossController)m_controller;
    }

    protected override void Update()
    {
        if (FSM.currentState is IntroState || FSM.currentState is TauntState)
            return;

        float disToTarget = Vector2.Distance(transform.position, target.position);

        if (!(FSM.currentState is MeleeAttackState) && !(FSM.currentState is SummonState))
        {
            FSM.ChangeState(new MeleeAttackState(this, target), true);
        }


        // enter attack range
        // melee attack
        if (disToTarget < AttackRange)
        {
            targetInAttackRange = true;

            //// currently not attack state
            //if (!(FSM.currentState is AttackState))
            //{
            //    Debug.Log("transit attack state");
            //    FSM.ChangeState(new AttackState(this, target), true);
            //}
        }

        // not in attack range
        // tentacle
        else
        {
            targetInAttackRange = false;

            //// currently not attack state
            //if (!(FSM.currentState is SummonState))
            //{
            //    Debug.Log("transit summon state");
            //    FSM.ChangeState(new SummonState(this, target), true);
            //}
        }
    }

    public override void Attack()
    {
        base.Attack();

        controller.MeleeAttack(target);

    }
}
