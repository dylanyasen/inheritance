﻿using UnityEngine;
using System.Collections;

public class ShroomBossController : BossController
{
    //private float jumpSpeed = 5f;
    //private float jumpDuration = 2f;

    public bool hasLanded { get; private set; }

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Update()
    {
        base.Update();
    }

    public void JumpAttack()
    {
        m_anim.SetTrigger("jump");

        m_entity._controller.DisableCollider();

        m_isAttacking = true;
        resetVelocity = false;

        hasLanded = false;
    }

    public void Land()
    {
        m_anim.SetTrigger("land");
        EnableCollider();
    }

    public void JumpAttackFinish()
    {
        resetVelocity = true;
        m_isAttacking = false;
    }

    public void Landed()
    {
        Debug.Log("has landed");
        hasLanded = true;
    }

}
