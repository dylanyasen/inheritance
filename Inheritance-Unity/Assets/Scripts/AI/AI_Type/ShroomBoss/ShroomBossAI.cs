﻿using UnityEngine;
using System.Collections;

public class ShroomBossAI : AIBoss
{
    public Transform footPos;

    private ShroomBossController controller;

    private JumpAttackState jumpAttackState;
    private AttackState attackState;

    protected override void Start()
    {
        base.Start();
        controller = (ShroomBossController)m_controller;

        // init states
        //jumpAttackState = 
        attackState = new AttackState(this, target);
    }

    protected override void Update()
    {
        //base.Update();

        if (FSM.currentState is IntroState || FSM.currentState is TauntState)
            return;

        float disToTarget = Vector2.Distance(transform.position, target.position);

        // trigger alert state
        if (disToTarget < AlertRange)
        {
            if (!(FSM.currentState is AlertState) && !(FSM.currentState is AttackState) && !(FSM.currentState is ChaseState) && !(FSM.currentState is JumpAttackState))
            {
                //Debug.Log("current state is " + FSM.currentState + ".  Change to alert state");
                FSM.ChangeState(new AlertState(this, target), true);
            }
        }

        // trigger idle state
        else if (disToTarget > AlertRange)
        {
            if (!(FSM.currentState is IdleState) && !(FSM.currentState is PatrolState) && !(FSM.currentState is JumpAttackState) && !(FSM.currentState is ChaseState))
            {
                //Debug.Log("out of alert range." + "current state is " + FSM.currentState + ".  Change to idle state");

                FSM.ChangeState(new IdleState(this), true);
            }
        }
    }

    public override void Alert()
    {
        //Debug.Log("this is melee simple alert");

        // player alert animation
        base.Alert();

        // randomize next attack state
        int i = Random.Range(0, 3);
        if (i == 0)
            FSM.ChangeState(new JumpAttackState(this, target.position, 2000, 1, 3000), true);

        else
            FSM.ChangeState(new ChaseState(this, target), true);


        Debug.Log("alert");

        //else if (i == 1)
        //   FSM.ChangeState(jumpAttackState, false);
    }

    public override void Attack()
    {
        base.Attack();

        m_controller.MeleeAttack(target);
    }


    public override void UnderAttack()
    {
        Debug.Log("this is melee simple under attack");
    }

    public override void Patrol()
    {
        Debug.Log("this is melee simple patrol");
        m_controller.MoveInRandDir();
    }

    public override void Idle()
    {
        Debug.Log("this is melee simple idle");
        m_controller.Idle();
    }

    public override void Chase(Transform target)
    {
        Debug.Log("this is melee simple chase");
        m_controller.Chase(target, 2);
    }
}
