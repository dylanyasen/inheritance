﻿using UnityEngine;
using System.Collections;

public class RangeSimpleAI : AIEntity
{
    protected override void Update()
    {
        base.Update();
    }

    public virtual void Alert()
    {
        Debug.Log("this is melee simple alert");
    }

    public virtual void Attack()
    {
        //m_controller.RangeAttack((target.position - m_entity.m_trans.position).normalized);
    }

    public virtual void UnderAttack()
    {
        Debug.Log("this is range simple under attack");
    }

    public virtual void Patrol()
    {
        Debug.Log("this is range simple patrol");
        m_controller.MoveInRandDir();
    }

    public virtual void Idle()
    {
        Debug.Log("this is range simple idle");
        m_controller.Idle();
    }

    public virtual void Chase(Transform target)
    {
        Debug.Log("this is range simple chase");
        m_controller.Chase(target, 2);
    }

}
