﻿using UnityEngine;
using System.Collections;

public class JumpState : AIState
{
    private AIEntity m_entity;

    public JumpState(AIEntity entity)
    {
        m_entity = entity;
    }

    public void Enter()
    {
        Debug.Log("jump state");

        m_entity.StartCoroutine("StateExecutor", this.Execute());
    }

    public IEnumerator Execute()
    {
        m_entity.Jump();

        while (m_entity.m_controller.m_isSpecialAttacking)
            yield return null;

        Debug.Log("jump state finished");

        yield return new WaitForSeconds(1);

        ExitToNextState();
    }

    public void ExitToNextState()
    {
        m_entity.FSM.ChangeState(new ChaseState(m_entity, m_entity.target), false);
    }

    public void Exit()
    {
        m_entity.StateTerminator(this.Execute());
    }
}
