﻿using UnityEngine;
using System.Collections;

public class MeleeAttackState : AIState
{
    protected AIBoss m_entity;
    private Transform target;

    public MeleeAttackState(AIEntity entity, Transform t)
    {
        this.m_entity = (AIBoss)entity;
        this.target = t;
    }

    public void Enter()
    {
        Debug.Log("enter melee state");
        m_entity.StartCoroutine("StateExecutor", this.Execute());
    }

    public IEnumerator Execute()
    {
        while (m_entity.targetInAttackRange)
        {
            m_entity.Attack();
            //Debug.Log("bite!" + Time.time);

            // or set attack cool down here
            yield return new WaitForSeconds(m_entity.AttackCoolDown);

            //yield return null;
        }

        yield return null;

        this.ExitToNextState();
    }

    public void Exit()
    {
        m_entity.StateTerminator(this.Execute());
        //Debug.Log ("exit alert state.");
    }

    public void ExitToNextState()
    {
        m_entity.FSM.ChangeState(new SummonState(m_entity, target), false);
    }

}
