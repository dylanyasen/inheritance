﻿using UnityEngine;
using System.Collections;

public class SummonState : AIState
{
    private AIBoss m_entity;
    private BossController m_controller;
    private Transform target;

    private Vector2 lastSummonPos;

    private Level curLvl;

    public SummonState(AIEntity entity, Transform target)
    {
        m_entity = (AIBoss)entity;
        m_controller = (BossController)m_entity.m_controller;
        this.target = target;
        this.lastSummonPos = Vector2.zero;
    }

    public void Enter()
    {
        Debug.Log("enter summon state");

        m_controller.SpecialAttack();

        m_entity.StartCoroutine("StateExecutor", this.Execute());
    }

    public IEnumerator Execute()
    {
        m_controller.Summon(target.position);

        while (m_entity.m_controller.m_isSpecialAttacking)
            yield return null;

        ExitToNextState();
    }

    public void ExitToNextState()
    {
        Debug.Log("exit summon state");

        m_entity.FSM.ChangeState(new MeleeAttackState(m_entity, target), false);
    }

    public void Exit()
    {
        Debug.Log("terminate summon state");

        m_entity.StateTerminator(this.Execute());
    }
}
