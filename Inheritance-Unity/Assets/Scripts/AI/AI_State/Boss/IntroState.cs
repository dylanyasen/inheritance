﻿using UnityEngine;
using System.Collections;

public class IntroState : AIState
{
    private AIBoss m_entity;
    private BossController m_controller;

    public IntroState(AIEntity entity)
    {
        m_entity = (AIBoss)entity;
        m_controller = (BossController)m_entity.m_controller;
    }

    public void Enter()
    {
        Debug.Log("intro state");
        m_entity.StartCoroutine("StateExecutor", this.Execute());
    }

    public IEnumerator Execute()
    {
        while (!m_controller.isAwakening)
            yield return null;

        //yield return new WaitForSeconds(1f);

        ExitToNextState();
    }

    public void ExitToNextState()
    {
        Debug.Log("exit from intro to taunt");

        m_entity.FSM.ChangeState(new TauntState(m_entity), false);
    }

    public void Exit()
    {
        m_entity.StateTerminator(this.Execute());
    }
}
