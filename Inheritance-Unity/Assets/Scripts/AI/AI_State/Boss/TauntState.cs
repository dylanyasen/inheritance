﻿using UnityEngine;
using System.Collections;

public class TauntState : AIState
{
    private AIBoss m_entity;
    private BossController m_controller;

    public TauntState(AIEntity entity)
    {
        m_entity = (AIBoss)entity;
        m_controller = (BossController)m_entity.m_controller;
    }

    public void Enter()
    {
        Debug.Log("enter taunt state");

        m_controller.OnAwaken();

        m_entity.StartCoroutine("StateExecutor", this.Execute());
    }

    public IEnumerator Execute()
    {
        Debug.Log("executing taunt state " + Time.time);


        while (m_controller.isAwakening)
            yield return null;

        yield return new WaitForSeconds(0.5f);

        m_controller.OnAwakenFinished();

        Debug.Log("exitting taunt state " + Time.time);

        ExitToNextState();
    }

    public void ExitToNextState()
    {
        Debug.Log("exit from taunt to idle");

        m_entity.FSM.ChangeState(new IdleState(m_entity), false);
    }

    public void Exit()
    {
        m_entity.StateTerminator(this.Execute());
    }
}
