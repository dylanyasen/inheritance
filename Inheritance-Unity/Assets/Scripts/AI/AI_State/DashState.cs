﻿using UnityEngine;
using System.Collections;

public class DashState : AIState
{
    protected AIEntity m_entity;

    public DashState(AIEntity entity)
    {
        this.m_entity = entity;
    }

    public void Enter()
    {
        m_entity.StartCoroutine("StateExecutor", this.Execute());
    }

    public IEnumerator Execute()
    {
        m_entity.Dash();


        while (m_entity.m_controller.m_isDashing)
            yield return null;
        //yield return new WaitForSeconds(2f);

        this.ExitToNextState();
    }

    public void Exit()
    {
        m_entity.StateTerminator(this.Execute());
        //Debug.Log("exit attack state");
    }

    public void ExitToNextState()
    {
        m_entity.FSM.ChangeState(new ChaseState(m_entity, m_entity.target), false);
    }

}
