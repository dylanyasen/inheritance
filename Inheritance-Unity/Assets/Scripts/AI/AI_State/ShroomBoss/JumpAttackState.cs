﻿using UnityEngine;
using System.Collections;

public class JumpAttackState : AIState
{
    private Vector2 jumpPos;
    private ShroomBossAI m_entity;
    private ShroomBossController entityController;

    private float jumpUpSpeed;
    private float jumpUpDuration;
    private float jumpDownSpeed;

    public JumpAttackState(AIEntity entity, Vector2 pos, float upSpeed, float upDuration, float downSpeed)
    {
        m_entity = (ShroomBossAI)entity;
        entityController = (ShroomBossController)m_entity.m_controller;

        jumpPos = pos;
        jumpUpSpeed = upSpeed;
        jumpUpDuration = upDuration;
        jumpDownSpeed = downSpeed;
    }

    public void Enter()
    {

        m_entity.StartCoroutine("StateExecutor", this.Execute());

    }

    public IEnumerator Execute()
    {
        // jump out of screen
        Debug.Log("jump");
        entityController.JumpAttack();
        yield return new WaitForSeconds(0.3f);
        entityController.SetVelocity(Vector2.up * jumpUpSpeed);

        // jump up for some time
        yield return new WaitForSeconds(jumpUpDuration);

        // fall
        // set x to target x
        entityController.SetXPos(jumpPos.x);
        entityController.SetVelocity(-Vector2.up * jumpDownSpeed);

        while (m_entity.footPos.position.y >= jumpPos.y)
            yield return null;

        // reset velocity
        entityController.SetVelocity(Vector2.zero);

        entityController.Land();

        while (!entityController.hasLanded)
            yield return null;

        entityController.JumpAttackFinish();

        ExitToNextState();
    }

    public void ExitToNextState()
    {
        m_entity.FSM.ChangeState(new IdleState(m_entity), false);
    }

    public void Exit()
    {
        m_entity.StateTerminator(this.Execute());
    }
}
