﻿using UnityEngine;
using System.Collections;

public class UnderAttackState : AIState
{
    protected AIEntity m_entity;

    public UnderAttackState(AIEntity entity)
    {
        this.m_entity = entity;
    }

    public void Enter()
    {
        Debug.Log("under attack state");
    }

    public IEnumerator Execute()
    {
        m_entity.UnderAttack();

        yield return null;

        //ExitToNextState();
    }

    public void ExitToNextState()
    {
        m_entity.FSM.ChangeState(new PatrolState(m_entity, m_entity.target), false);
    }

    public void Exit()
    {
        m_entity.StateTerminator(this.Execute());
    }
}
