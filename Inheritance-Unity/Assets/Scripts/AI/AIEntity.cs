﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Enemy))]

public class AIEntity : MonoBehaviour
{
    public StateMachine FSM { get; private set; }

    public Transform target { get; set; }
    public Transform patrolPoint { get; set; }

    public float AlertRange = 5;
    public float AttackRange = 1;
    public float AttackCoolDown = 1;

    public Enemy m_entity { get; private set; }
    public EntityStats m_stat { get; private set; }
    public EnemyController m_controller { get; private set; }
    public float disToTarget { get; protected set; }
    // public Animator m_anim { get; private set; }


    protected void Awake()
    {

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, AlertRange);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AttackRange);
    }

    protected virtual void Start()
    {

        FSM = new StateMachine(this);
        m_entity = GetComponent<Enemy>();
        m_stat = m_entity.m_stats;
        m_controller = (EnemyController)m_entity.m_controller;

    }

    protected virtual void Update()
    {
        if (FSM.currentState is UnderAttackState)
            return;

        // no target
        if (target == null)
        {
            if (!(FSM.currentState is IdleState) && !(FSM.currentState is PatrolState))
            {
                FSM.ChangeState(new IdleState(this), true);

                //Debug.Log("target null");
            }

            return;
        }

        disToTarget = Vector2.Distance(transform.position, target.position);

        // trigger alert state
        if (disToTarget < AlertRange)
        {
            if (!(FSM.currentState is AlertState) && !(FSM.currentState is AttackState) && !(FSM.currentState is ChaseState))
            {
                //Debug.Log("current state is " + FSM.currentState + ".  Change to alert state");
                FSM.ChangeState(new AlertState(this, target), true);
            }
        }

        // trigger idle state
        else if (disToTarget > AlertRange)
        {
            if (!(FSM.currentState is IdleState) && !(FSM.currentState is PatrolState))
            {

                //Debug.Log("out of alert range." + "current state is " + FSM.currentState + ".  Change to idle state");

                FSM.ChangeState(new IdleState(this), true);
            }
        }
    }

    public IEnumerator StateExecutor(IEnumerator state)
    {
        while (state.MoveNext())
            yield return state.Current;
    }

    public void StateTerminator(IEnumerator state)
    {
        // change this later
        // this will mess up other irrelevant coroutines
        StopAllCoroutines();
    }

    public virtual void Idle()
    {
        //Debug.Log("ai base idle");
    }

    public virtual void Attack()
    {
        //Debug.Log("ai base attack");
    }

    public virtual void UnderAttack()
    {
        //Debug.Log("ai base under attack");
    }

    public virtual void Alert()
    {
        //Debug.Log("ai base alert");
    }

    public virtual void Patrol()
    {
        //Debug.Log("ai base patrol");
    }

    public virtual void Chase(Transform target)
    {
        //Debug.Log("ai base Chase");
    }


    public virtual void Dash()
    {

    }

    public virtual void Jump()
    {

    }
}
