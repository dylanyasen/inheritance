﻿using UnityEngine;

public class SetTheme_GUI_Button : MonoBehaviour
{
    public LevelManager.LevelTheme theme;

    public void SetThemeForLevelManager()
    {
        GM.instance._levelManager.SetTheme(theme);
    }
}
