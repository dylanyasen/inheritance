﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerStatGui : MonoBehaviour
{
    public Image hpBar;
    public Image spBar;
    public Image face;

    private Player player;

    public void UpdateHp(float hp)
    {
        hpBar.fillAmount = hp / player.m_stats.MaxHP;
    }

    public void UpdateSp(float sp)
    {
        //Debug.Log("update sp " + sp);


        spBar.fillAmount = sp / player.m_stats.MaxSP;

        //Debug.Log(spBar.fillAmount);
    }

    public void SetFace(Sprite sp)
    {
        face.sprite = sp;
    }

    public void SetPlayer(Player p)
    {
        player = p;
    }

}
