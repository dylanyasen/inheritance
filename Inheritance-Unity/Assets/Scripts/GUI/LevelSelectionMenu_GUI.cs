﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelSelectionMenu_GUI : MonoBehaviour
{
    public Sprite[] bgImg;
    public Sprite[] nameImg;
    public string[] levelName;

    public Image levelImg;
    public Image levelNameImg;

    public SetTheme_GUI_Button setThemeScript;

    private int currentLevel;

    public void Start()
    {
        currentLevel = 0;
        SetImg();
    }

    public void NextLevel()
    {
        currentLevel++;

        if (currentLevel >= bgImg.Length)
            currentLevel = 0;

        SetImg();
    }

    public void PrevLevel()
    {
        currentLevel--;

        if (currentLevel < 0)
            currentLevel = bgImg.Length - 1;

        SetImg();
    }

    private void SetImg()
    {
        Debug.Log("bg " + bgImg.Length);
        Debug.Log("name " + nameImg.Length);

        levelImg.sprite = bgImg[currentLevel];
        levelNameImg.sprite = nameImg[currentLevel];

        if (levelName[currentLevel] == "GlowingMushroom")
            setThemeScript.theme = LevelManager.LevelTheme.GlowingMushroom;
        else if (levelName[currentLevel] == "Forest")
            setThemeScript.theme = LevelManager.LevelTheme.Forest;


    }
}
