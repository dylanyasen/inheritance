﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeBackgorundImage_GUI : MonoBehaviour
{
    private Image bg;
    private LevelManager.LevelTheme theme;

    void Awake()
    {
        bg = GetComponent<Image>();
    }

    void Start()
    {
        theme = GM.instance._levelManager.theme;

        SetImage();
    }

    void SetImage()
    {
        bg.sprite = Resources.Load<Sprite>(Constants.BackGroundImagePath + theme.ToString());
    }

}
