﻿using UnityEngine;
using System.Collections;

public class DeathOverLay_GUI : MonoBehaviour
{
    public GameObject spouse_button;
    public GameObject child_button;

    public void Start()
    {
        string p1 = GM.instance._serilizationManager.LoadParent(0);
        string p2 = GM.instance._serilizationManager.LoadParent(1);

        Debug.Log(p1 + " " + p2);

        // only have one parent
        // can't pick child
        if (p2 == "NULL")
            child_button.SetActive(false);
    }

    public void PickSpouse()
    {
        GM.instance.PickSpouse();
    }

    public void PickChild()
    {
        GM.instance.PickChild();
    }

}
