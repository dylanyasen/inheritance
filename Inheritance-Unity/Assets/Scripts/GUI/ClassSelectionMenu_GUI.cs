﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ClassSelectionMenu_GUI : MonoBehaviour
{
    public Sprite[] classSprite;
    public Sprite[] nameSprite;
    public string[] className;

    public Image classImg;
    public Image classnameSprite;

    private int currentIndex;

    public void Start()
    {
        currentIndex = 0;
        SetImg();
    }

    public void NextClass()
    {
        currentIndex++;

        if (currentIndex >= classSprite.Length)
            currentIndex = 0;

        SetImg();
    }

    public void PrevClass()
    {
        currentIndex--;

        if (currentIndex < 0)
            currentIndex = classSprite.Length - 1;

        SetImg();
    }

    private void SetImg()
    {
        classImg.sprite = classSprite[currentIndex];
        classnameSprite.sprite = nameSprite[currentIndex];
    }

    public void SavePlayerName()
    {
        GM.instance._serilizationManager.SavePlayer(className[currentIndex]);
    }
}
