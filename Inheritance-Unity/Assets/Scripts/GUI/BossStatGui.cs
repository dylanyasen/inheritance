﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BossStatGui : MonoBehaviour
{
    public Image hpBar;
    private Boss boss;

    public void UpdateHp(float hp)
    {
        hpBar.fillAmount = hp / boss.m_stats.MaxHP;
    }

    public void SetBoss(Boss b)
    {
        boss = b;
    }
}
