﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Transform followTarget_t;

    [Range(1, 10)]
    public int speed = 3;

    [Range(1, 10)]
    public int camSizeScaler = 1;

    private int zAxis = -10;
    private Transform m_trans;

    private Vector2 vec;
    private Camera m_cam;

    private bool isFollowing = true;

    // shake
    private bool isShaking;
    private float amplitude = 1;
    private float duration = 0f;
    private float shakeTimer = 0;

    private Vector2 cachePos;

    void Awake()
    {
        m_trans = GetComponent<Transform>();
        m_cam = GetComponent<Camera>();

        m_cam.orthographicSize = Mathf.RoundToInt(Screen.height / 2 / 1);
        //m_cam.orthographicSize = Mathf.RoundToInt(768f / 2 / 1);
    }

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Shake();
        }

        if (!isFollowing)
            return;

        // Camera Shake
        else if (isShaking)
            OnShake();

        else
        {
            cachePos = transform.position;
            FollowPlayer();
        }
    }

    void FollowPlayer()
    {
        // get the target
        if (followTarget_t == null)
        {
            Debug.Log("null");
            followTarget_t = GameObject.FindGameObjectWithTag("Player").transform;
            return;
        }

        vec = Vector2.Lerp(m_trans.position, followTarget_t.position, speed * Time.deltaTime);

        string xStr = vec.x.ToString("0.0");
        string yStr = vec.y.ToString("0.0");

        float x = float.Parse(xStr);
        float y = float.Parse(yStr);

        //m_trans.position = new Vector3(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y), zAxis);

        m_trans.position = new Vector3(x, y, zAxis);
    }

    void OnShake()
    {
        if (shakeTimer < duration)
        {
            Vector3 vec = cachePos + Random.insideUnitCircle * amplitude;
            vec.z = -10;
            transform.position = vec;
            shakeTimer += Time.deltaTime;
        }
        else
        {
            shakeTimer = 0;
            isShaking = false;
        }
    }

    public void Shake(float amp = 5, float dur = 0.1f)
    {
        duration = dur; // += additive shake  // = 
        amplitude = amp;

        isShaking = true;
    }

    // 
    public void DirectionalShake(float x)
    {

    }

    public void GoToPos(Vector2 pos)
    {
        Vector3 v = new Vector3(pos.x, pos.y, -10);

        m_trans.position = v;
    }

    public void SetOffset(float x, float y)
    {
        Vector2 v = m_trans.position;

        Debug.Log(x + " " + y);
        Debug.Log(v);
        v.x += x;
        v.y += y;
        Debug.Log(v);
        m_trans.position = v;
    }

    public void SwitchIsFollowing(bool follow)
    {
        isFollowing = follow;
    }

    public void SetFollow(Transform t)
    {
        followTarget_t = t;
    }

}
