﻿using UnityEngine;
using System.Collections;

public class CurvedArrow : ProjectileBase
{
    private float duration = 1.5f;
    private float timer;

    protected override void Awake()
    {
        base.Awake();
    }

    public override void Fire(Vector2 dir)
    {
        int gravity = 0;
        if (dir == Vector2.up || dir == Vector2.up)
            gravity = 0;
        else
            gravity = 30;

        if (dir.x == 1)
        {
            dir.x = dir.x * Mathf.Cos(15 * Mathf.Deg2Rad) - dir.y * Mathf.Sin(-15 * Mathf.Deg2Rad);
            dir.y = dir.x * Mathf.Sin(15 * Mathf.Deg2Rad) + dir.y * Mathf.Cos(-15 * Mathf.Deg2Rad);
        }

        else if (dir.x == -1)
        {
            dir.x = dir.x * Mathf.Cos(-15 * Mathf.Deg2Rad) - dir.y * Mathf.Sin(15 * Mathf.Deg2Rad);
            dir.y = dir.x * Mathf.Sin(-15 * Mathf.Deg2Rad) + dir.y * Mathf.Cos(15 * Mathf.Deg2Rad);
        }

        dir.Normalize();

        body.gravityScale = gravity;

        base.Fire(dir);
    }

    protected override void FixedUpdate()
    {
        // rotate
        float angle = Mathf.Atan2(body.velocity.normalized.y, body.velocity.normalized.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, angle);

        // destory timer
        timer += Time.deltaTime;
        if (timer >= duration)
            Destroy(gameObject);
    }

    protected override void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log(other.gameObject.tag);

        if (other.gameObject.tag == "Enemy")
        {
            Debug.Log("hit enemy");

            Entity e = other.gameObject.GetComponent<Entity>();
            e.ApplyDamage(dmg);
        }

        Destroy(gameObject);
    }
}
