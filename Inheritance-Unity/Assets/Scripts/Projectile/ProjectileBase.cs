﻿using UnityEngine;
using System.Collections;

public abstract class ProjectileBase : MonoBehaviour
{
    protected Entity m_entity { get; private set; }
    protected int dmg { get; private set; }
    //protected int speed;
    protected Rigidbody2D body { get; private set; }
    protected Transform trans { get; private set; }
    protected SpriteRenderer spRenderer { get; private set; }
    protected BoxCollider2D collider { get; private set; }
    protected bool isEnmeyProjectile { get; private set; }
    public float speed = 600;

    protected bool isFired { get; private set; }


    //protected 

    protected virtual void Awake()
    {

        body = GetComponent<Rigidbody2D>();
        trans = GetComponent<Transform>();
        spRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider2D>();
    }

    protected virtual void Start()
    {
        //collider.size = spRenderer.bounds.size;
    }

    protected abstract void FixedUpdate();

    protected abstract void OnCollisionEnter2D(Collision2D other);

    public virtual void Fire(Vector2 dir)
    {
        isFired = true;
        body.velocity = dir * speed;
    }

    public void SetDmg(int dmg)
    {
        this.dmg = dmg;
    }

    public void SetSpeed(int speed)
    {
        this.speed = speed;
    }

    public void SetEnemyProj()
    {
        isEnmeyProjectile = true;
    }

}
