﻿using UnityEngine;
using System.Collections;

public class ThrowObject : ProjectileBase
{
    public bool rotate { get; private set; }
    public int weaponID { get; private set; }

    private float rotateSpeed = 1000;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void FixedUpdate()
    {
        if (rotate)
            body.MoveRotation(body.rotation - rotateSpeed * Time.fixedDeltaTime);
    }

    protected override void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log(other.gameObject.tag);

        if (other.gameObject.tag == "Enemy")
        {
            Debug.Log("hit enemy");

            Entity e = other.gameObject.GetComponent<Entity>();
            e.ApplyDamage(dmg);
        }

        //rotate = false;
        // reset velocity
        //body.velocity = Vector2.zero;
        GameObject pickup = Instantiate(Resources.Load<GameObject>(Constants.EntityPrefabPath + "Pickup"), transform.position, Quaternion.identity) as GameObject;
        pickup.GetComponent<Pickup>().Init(weaponID);

        Destroy(gameObject);
    }

    public void SetSprite(Sprite sp)
    {
        Debug.Log(sp);
        Debug.Log(GetComponent<Renderer>());
        spRenderer.sprite = sp;
    }

    public void SetRotate(bool b)
    {
        rotate = b;
    }

    public void SetRotateSpeed(float speed)
    {
        rotateSpeed = speed;
    }

    public void SetWeaponID(int id)
    {
        weaponID = id;
    }
}

