﻿using UnityEngine;
using System.Collections;

public class ProjectileWithParticle : ProjectileBase
{
    private ParticleSystem[] particles;

    protected override void Awake()
    {
        base.Awake();

        particles = GetComponentsInChildren<ParticleSystem>();

    }

    public override void Fire(Vector2 dir)
    {
        if (dir == Vector2.right)
            transform.rotation = Quaternion.Euler(0, 0, 90);

        else if (dir == -Vector2.right)
            transform.rotation = Quaternion.Euler(0, 0, -90);

        else if (dir == Vector2.up)
            transform.rotation = Quaternion.Euler(0, 0, 180);

        for (int i = 0; i < particles.Length; i++)
            particles[i].Play();

        base.Fire(dir);
    }

    public void Fire()
    {
        base.Fire((Vector2)(-transform.up));
        for (int i = 0; i < particles.Length; i++)
            particles[i].Play();
    }

    protected override void FixedUpdate()
    {
    }

    protected override void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("hit " + other.gameObject.name);

        if (other.gameObject.tag == "Enemy" && !isEnmeyProjectile)
        {
            Debug.Log("hit enemy");

            Entity e = other.gameObject.GetComponent<Entity>();
            e.ApplyDamage(dmg);

            Destroy(gameObject);
        }

        else if (other.gameObject.tag == "Player" && isEnmeyProjectile)
        {
            GM.instance._player.ApplyDamage(dmg);
            Destroy(gameObject);
        }

        else if (other.gameObject.layer == LayerMask.NameToLayer("obstacle")){
            Destroy(gameObject);
        }
    }
}
