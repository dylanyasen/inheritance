﻿using UnityEngine;
using System.Collections;

public class BossController : EnemyController
{
    //public bool isAwaken;// { get; protected set; }
    public bool isAwakening;// { get; private set; }
    public bool hasAwaken { get; private set; }
    private Boss m_boss;

    protected virtual void Start()
    {

        m_boss = GetComponent<Boss>();
    }

    void OnBecameVisible()
    {
        Debug.Log("visible!");

        if (hasAwaken)
            return;

        isAwakening = true;
        hasAwaken = true;
        GM.instance._camController.SwitchIsFollowing(false);
        GM.instance._camController.GoToPos(m_trans.position);
        GM.instance._player.m_controller.SetCanControl(false);

        GameObject hpCanvas = Resources.Load<GameObject>(Constants.GuiPrefabsPath + "BossHpCanvas");
        GameObject g = Instantiate(hpCanvas, Vector2.zero, Quaternion.identity) as GameObject;
        BossStatGui bs = g.GetComponent<BossStatGui>();
        bs.SetBoss(m_boss);

        m_boss.SetStatGUI(bs);

        //m_anim.SetBool("isAwaken", true);
    }

    void OnBecameInvisible()
    {
        Debug.Log("Invisible!");

    }

    public void OnAwaken()
    {
        isAwakening = true;
        // play intro animation
        m_anim.SetBool("isAwaking", true);
    }

    public void OnAwakenFinished()
    {
        GM.instance._camController.SwitchIsFollowing(true);
        GM.instance._player.m_controller.SetCanControl(true);
    }

    public void TauntFinished()
    {
        Debug.Log("TAUNT FINISHED");

        isAwakening = false;
        m_anim.SetBool("isAwaking", false);
    }
}
