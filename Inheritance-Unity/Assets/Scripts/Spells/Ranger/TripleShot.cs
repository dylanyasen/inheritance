﻿using UnityEngine;
using System.Collections;

public class TripleShot : Spell
{
    private GameObject arrow;

    public TripleShot(Player p)
        : base(p)
    {
        spCost = 40;
    }

    public override void Invoke()
    {
        if (!m_player.m_stats.LoseSP(spCost))
            return;

        //m_player.m_anim.SetBool("isAttacking", true);

        // get arrow obj from equipped weapon
        arrow = ((RangeWeapon)m_player.weaponManager.currentWeapon)._projectile;
        if (arrow == null)
            return;

        // player animation
        m_player.m_anim.SetBool("isAttacking", true);


        // directions for arrows
        Vector2[] dir = new Vector2[3];
        dir[0] = m_player.GetDir();

        int gravity = 0;

        // veritcal direction
        if (dir[0] == Vector2.up || dir[0] == -Vector2.up)
        {
            // one arrow 15deg to the left
            dir[1].x = dir[0].x * Mathf.Cos(15 * Mathf.Deg2Rad) - dir[0].y * Mathf.Sin(-15 * Mathf.Deg2Rad);
            dir[1].y = dir[0].x * Mathf.Sin(15 * Mathf.Deg2Rad) + dir[0].y * Mathf.Cos(-15 * Mathf.Deg2Rad);

            // one arrow 15deg to the right
            dir[2].x = dir[0].x * Mathf.Cos(-15 * Mathf.Deg2Rad) - dir[0].y * Mathf.Sin(15 * Mathf.Deg2Rad);
            dir[2].y = dir[0].x * Mathf.Sin(-15 * Mathf.Deg2Rad) + dir[0].y * Mathf.Cos(15 * Mathf.Deg2Rad);

            // no gravity when shooting verticlly
            gravity = 0;
        }

        // right
        else if (dir[0] == Vector2.right)
        {
            dir[0].x = dir[0].x * Mathf.Cos(20 * Mathf.Deg2Rad) - dir[0].y * Mathf.Sin(-20 * Mathf.Deg2Rad);
            dir[0].y = dir[0].x * Mathf.Sin(20 * Mathf.Deg2Rad) + dir[0].y * Mathf.Cos(-20 * Mathf.Deg2Rad);

            // one arrow 15deg to the left
            dir[1].x = dir[0].x * Mathf.Cos(5 * Mathf.Deg2Rad) - dir[0].y * Mathf.Sin(-5 * Mathf.Deg2Rad);
            dir[1].y = dir[0].x * Mathf.Sin(5 * Mathf.Deg2Rad) + dir[0].y * Mathf.Cos(-5 * Mathf.Deg2Rad);

            // one arrow 15deg to the right
            dir[2].x = dir[0].x * Mathf.Cos(-5 * Mathf.Deg2Rad) - dir[0].y * Mathf.Sin(5 * Mathf.Deg2Rad);
            dir[2].y = dir[0].x * Mathf.Sin(-5 * Mathf.Deg2Rad) + dir[0].y * Mathf.Cos(5 * Mathf.Deg2Rad);

            // no gravity when shooting verticlly
            gravity = 50;
        }

        // left
        else if (dir[0] == -Vector2.right)
        {
            dir[0].x = dir[0].x * Mathf.Cos(-20 * Mathf.Deg2Rad) - dir[0].y * Mathf.Sin(20 * Mathf.Deg2Rad);
            dir[0].y = dir[0].x * Mathf.Sin(-20 * Mathf.Deg2Rad) + dir[0].y * Mathf.Cos(20 * Mathf.Deg2Rad);

            // one arrow 15deg to the left
            dir[1].x = dir[0].x * Mathf.Cos(5 * Mathf.Deg2Rad) - dir[0].y * Mathf.Sin(-5 * Mathf.Deg2Rad);
            dir[1].y = dir[0].x * Mathf.Sin(5 * Mathf.Deg2Rad) + dir[0].y * Mathf.Cos(-5 * Mathf.Deg2Rad);

            // one arrow 15deg to the right
            dir[2].x = dir[0].x * Mathf.Cos(-5 * Mathf.Deg2Rad) - dir[0].y * Mathf.Sin(5 * Mathf.Deg2Rad);
            dir[2].y = dir[0].x * Mathf.Sin(-5 * Mathf.Deg2Rad) + dir[0].y * Mathf.Cos(5 * Mathf.Deg2Rad);

            // no gravity when shooting verticlly
            gravity = 50;
        }

        // create arrows
        for (int i = 0; i < 3; i++)
        {
            GameObject g = MonoBehaviour.Instantiate(arrow, m_player.m_trans.position, Quaternion.identity) as GameObject;
            g.GetComponent<CurvedArrow>().SetDmg(m_player.m_stats.cur_dmg);
            g.GetComponent<CurvedArrow>().Fire(dir[i] * 800);
        }
    }
}
