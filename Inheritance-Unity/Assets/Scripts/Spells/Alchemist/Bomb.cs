﻿using UnityEngine;
using System.Collections;

public class Bomb : StaticEntity
{
    float duration = 2;
    int _dmg;

    public AudioClip explodeSfx;
    public AudioClip fuseBuringSfx;

    float timer = 0;

    void Start()
    {
        explodeSfx = Resources.Load<AudioClip>(Constants.sfxFolder + "Bomb/explode");
        fuseBuringSfx = Resources.Load<AudioClip>(Constants.sfxFolder + "Bomb/fuseBurning");

        audioSource.clip = fuseBuringSfx;
        audioSource.Play();
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer > duration)
            Explode();
    }

    void Explode()
    {
        Debug.Log("explode");

        // apply dmg
        // push back
        // play explode sfx

        //audioSource.PlayOneShot(explodeSfx);
        AudioSource.PlayClipAtPoint(explodeSfx, m_trans.position);

        Destroy(gameObject);
    }
}
