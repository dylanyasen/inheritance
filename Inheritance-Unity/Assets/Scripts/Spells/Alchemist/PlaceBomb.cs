﻿using UnityEngine;
using System.Collections;

public class PlaceBomb : Spell
{
    private GameObject bombPrefab;
    private Bomb bomb;


    public PlaceBomb(Player p)
        : base(p)
    {
        bombPrefab = Resources.Load<GameObject>(Constants.SpellPrefabPath + "Bomb");

        spCost = 40;
    }

    public override void Invoke()
    {
        if (!m_player.m_stats.LoseSP(spCost))
            return;

        // create the obj to throw
        GameObject go = MonoBehaviour.Instantiate(bombPrefab, m_player.GetPos(), Quaternion.identity) as GameObject;
        bomb = go.GetComponent<Bomb>();


    }
}
