﻿using UnityEngine;
using System.Collections;

public abstract class Spell
{
    //protected GameObject effectPrefab;
    protected Player m_player;

    protected int spCost;

    public Spell(Player p)
    {
        m_player = p;
    }

    public abstract void Invoke();

}
