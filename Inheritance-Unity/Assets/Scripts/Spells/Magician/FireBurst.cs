﻿using UnityEngine;
using System.Collections;

public class FireBurst : Spell
{
    private GameObject fireObj;

    public FireBurst(Player p)
        : base(p)
    {
        spCost = 40;

        fireObj = Resources.Load<GameObject>(Constants.FxPrefabPath + "FireCloud");
    }

    public override void Invoke()
    {
        if (!m_player.m_stats.LoseSP(spCost))
            return;

        // player animation
        m_player.m_anim.SetBool("isAttacking", true);

        Vector2 centerTile = GM.instance._levelManager.GetCurrentLevel().GetCurrentTileIndex(m_player.GetPos());

        Tile[,] tiles = GM.instance._levelManager.GetCurrentLevel().Tiles;

        Vector2[] tileIndex = new Vector2[8];

        for (int i = 0; i < 8; i++)
            tileIndex[i] = centerTile;

        // start from top left
        tileIndex[0].x -= 1;
        tileIndex[0].y += 1;

        tileIndex[1].x -= 0;
        tileIndex[1].y += 1;

        tileIndex[2].x += 1;
        tileIndex[2].y += 1;

        tileIndex[3].x -= 1;
        tileIndex[3].y += 0;

        tileIndex[4].x += 1;
        tileIndex[4].y += 0;

        tileIndex[5].x -= 1;
        tileIndex[5].y -= 1;

        tileIndex[6].x -= 0;
        tileIndex[6].y -= 1;

        tileIndex[7].x += 1;
        tileIndex[7].y -= 1;

        for (int i = 0; i < 8; i++)
        {
            if (tiles[(int)tileIndex[i].x, (int)tileIndex[i].y] == null)
                continue;

            Vector2 pos = tiles[(int)tileIndex[i].x, (int)tileIndex[i].y].GetPos();

            GameObject g = MonoBehaviour.Instantiate(fireObj, pos, Quaternion.Euler(-90, 0, 0)) as GameObject;
        }
    }
}
