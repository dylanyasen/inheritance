﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ThrowWeapon : Spell
{
    private Weapon weapon;
    private Transform weaponAttachPoint;
    private GameObject throwPrefab;
    private ThrowObject throwObj;

    public ThrowWeapon(Player p)
        : base(p)
    {
        // temp
        throwPrefab = Resources.Load<GameObject>(Constants.SpellPrefabPath + "ThrowWeapon");

        spCost = 10;
    }

    public override void Invoke()
    {
        if (!m_player.m_stats.LoseSP(spCost))
            return;

        // player animation
        m_player.m_anim.SetBool("isAttacking", true);


        // get the current equipped weapon from player
        weapon = m_player.weaponManager.currentWeapon;
        weaponAttachPoint = m_player.weaponManager.WeaponPoint_L;

        if (weapon == null)
            return;

        Debug.Log("thorw weapon");

        // create the obj to throw
        GameObject go = MonoBehaviour.Instantiate(throwPrefab, weaponAttachPoint.position, weaponAttachPoint.rotation) as GameObject;
        throwObj = go.GetComponent<ThrowObject>();

        // set vals for projectile
        throwObj.SetSprite(weapon.itemSprite);
        throwObj.SetDmg(weapon._dmg);
        throwObj.SetRotate(true);

        throwObj.SetSpeed(500);
        throwObj.Fire(m_player.GetDir());

        throwObj.SetWeaponID(weapon.itemID);

        // unequip weapon
        m_player.weaponManager.UnEquip(weapon);

        List<Item> items = m_player.charPanel.items;

        foreach (CharPanelSlot slot in m_player.charPanel.charSlots)
        {
            if (items[slot.index] == weapon)
                slot.UnequipInSlotItem();
        }
    }
}
